from firedrake import *
from linear_mean_flow import MeanFlowLinearSlice

# set parameters
L = 300000.
H = 10000.
nlayers = 10
ncolumns = 300
dt = 12.
nits = 5
alpha = 0.5
Nsq = 1.e-2**2
csq = 300.**2
U = 20.

# set up initial conditions
u0 = Expression(("0.0","0.0"))
v0 = Expression("0.0")
p0 = Expression("0.0")
b0 = Expression("0.01*sin(pi*x[1]/10000.)/(1+pow(x[0]-150000.,2)/pow(5000.,2))")

Model = MeanFlowLinearSlice(L=L, H=H, nlayers=nlayers, ncolumns=ncolumns, pvertical=2, phorizontal=2, dt=dt, nits=nits, alpha=alpha, Nsq=Nsq, csq=csq, f=0.0, u0_exp=u0, v0_exp=v0, p0_exp=p0, b0_exp=b0, U=U)

Model.solve(t=0.0, end=250*dt, Tdump=dt, filename='mean_flow')
