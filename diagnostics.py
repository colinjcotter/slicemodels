from firedrake import *
from utility_functions import projection_solver
import numpy as np

def max(f):
    fmax = op2.Global(1, [-1000], dtype=float)
    op2.par_loop(op2.Kernel("""void maxify(double *a, double *b)
    {
    a[0] = a[0] < fabs(b[0]) ? fabs(b[0]) : a[0];
    }""", "maxify"),
                 f.dof_dset.set, fmax(op2.MAX), f.dat(op2.READ))
    return fmax.data[0]

def min(f):
    fmin = op2.Global(1, [1000], dtype=float)
    op2.par_loop(op2.Kernel("""void minify(double *a, double *b)
    {
    a[0] = a[0] > fabs(b[0]) ? fabs(b[0]) : a[0];
    }""", "minify"),
                 f.dof_dset.set, fmin(op2.MIN), f.dat(op2.READ))
    return fmin.data[0]

def rms(f):
    V = FunctionSpace(f.function_space().mesh(), "DG", 1)
    c = Function(V)
    c.assign(1)
    rms = sqrt(assemble((dot(f,f))*dx)/assemble(c*dx))
    return rms

def kinetic_energy(p, old):
    oldu, oldv, oldb, oldp = old.vars.split()
    kinetic = assemble(0.5*(dot(oldu,oldu)+oldv*oldv)*dx)
    return kinetic

def potential_energy(p, old):
    oldu, oldv, oldb, oldp = old.vars.split()
    potential = assemble(0.5*dot(oldb,oldb)/p.Nsq*dx)
    return potential

def internal_energy(p, old):
    oldu, oldv, oldb, oldp = old.vars.split()
    internal = assemble(0.5*dot(oldp,oldp)/p.csq*dx)
    return internal

def total_energy(p, old):
    total = (
        kinetic_energy(p, old) + potential_energy(p, old) 
        + internal_energy(p, old)
    )
    return total

def total_energy_incompressible(p, old):
    total = (
        kinetic_energy(p, old) + potential_energy(p, old) 
    )
    return total

def courant_number(u, dt, dL):
    umax = max(u)
    courant = umax*dt/dL
    return courant

def kinetic_energy_u(p, old):
    oldu, oldv, oldb, oldp = old.vars.split()
    kinetic = assemble(0.5*dot(oldu,oldu)*dx)
    return kinetic

def kinetic_energy_v(p, old):
    oldu, oldv, oldb, oldp = old.vars.split()
    kinetic = assemble(0.5*(oldv*oldv)*dx)
    return kinetic
