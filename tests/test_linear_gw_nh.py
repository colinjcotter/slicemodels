import pytest
import sys

from slicemodels import SliceConfiguration, LinearSlice
from firedrake import *

def gwnh():

    L = 200000.
    b0_exp = Expression("0.01*sin(pi*x[1]/10000.)/(1+pow(x[0]-0.5*L,2)/pow(5000.,2))",L=L)

    ksp_atol = 1.e-04
    ksp_rtol = 1.e-04
    dt = 100.

    setup = SliceConfiguration(
        L=L, H=10000., nlayers=5, ncolumns=20,
        dt=dt, end=5*dt, Tdump=6*dt,
        Nsq=1.e-4, nits=2, 
        initial_conditions = { "b" : b0_exp},
        ksp_atol=ksp_atol, ksp_rtol=ksp_rtol,
        ksp_monitor_true_residual=False,
        dirname='tests/linear_gw_nh')

    Model = LinearSlice(setup)

    Data, dirs = Model.run()

    ene_list = Data["total_energy"]
    return ene_list, ksp_rtol


def run_total_energy():
    
    ene_list, ksp_rtol = gwnh()
    print ene_list[0], ene_list[-1]
    # get initial and final total energy:
    ene_init = ene_list[0]
    ene_fin = ene_list[-1]

    # check energy of same order of magnitude as previous calculation
    assert abs(ene_init - 1.89146e7) < 1e2

    # check energy doesn't change too much:
    assert abs(ene_fin - ene_init) < 1.e-04*ene_init


def test_total_energy():
    run_total_energy()


@pytest.mark.parallel(nprocs=2)
def test_total_energy_parallel():
    run_total_energy()

