import pytest
import sys

from nonlinear_slice import NonlinearSlice, NonlinearSliceConfiguration
from firedrake import *

def gwnh():

    L = 20000.
    u0_exp = Expression(("20.0","0.0"))
    b0_exp = Expression("0.01*sin(pi*x[1]/10000.)/(1+pow(x[0]-0.5*L,2)/pow(5000.,2))", L=L)

    ksp_atol = 1.e-04
    ksp_rtol = 1.e-04
    dt=12.

    setup = NonlinearSliceConfiguration(
        L=L, H=10000., nlayers=5, ncolumns=10,
        dt=dt, end=2*dt, Tdump=4*dt, Nsq=1.e-4,
        initial_conditions = {"uw": u0_exp, "b": b0_exp, "p": "hydrostatic"},
        advection_options = {"uw" : {"formulation" : "vec_invariant"}},
        ksp_atol=ksp_atol, ksp_rtol=ksp_rtol, ksp_monitor_true_residual=False,
        dirname='tests/nonlinear_gw_nh')

    Model = NonlinearSlice(setup)

    Data, dirs = Model.run()

    ene_list = Data["total_energy"]
    print ene_list
    return ene_list, ksp_rtol


def run_total_energy():
    
    ene_list, ksp_rtol = gwnh()

    # get initial and final total energy:
    print ene_list[0], ene_list[-1]
    ene_init = ene_list[0]
    ene_fin = ene_list[-1]

    # check energy of same order of magnitude as previous calculation
    assert abs(ene_init - 4.00194e10) < 1e6
    # check energy doesn't change too much:
    assert abs(ene_fin - ene_init) < ksp_rtol*ene_init


def test_total_energy():
    run_total_energy()


@pytest.mark.parallel(nprocs=2)
def test_total_energy_parallel():
    run_total_energy()


