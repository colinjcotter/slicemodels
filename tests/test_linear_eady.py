import pytest
import sys
import inifns

from linear_eady_model import LinearEadySliceConfiguration, LinearEadySlice
from firedrake import *

def eady():

    L = 1000000.
    H = 10000.
    f = 1.0e-4
    a = -7.5
    Nsq = 2.5e-05
    Bu = 0.5

    nlayers = 50
    ncolumns = 50
    dt = 100.

    alpha = 0.5
    beta = 1.0

    dt = beta*dt
    f = f/beta
    L = beta*L

    template_s = inifns.template_target_strings()
    b0_exp = Expression(template_s,a=a,Nsq=Nsq,Bu=Bu,H=H,L=L)

    setup = LinearEadySliceConfiguration(
        L=2*L,H=H,nlayers=nlayers,ncolumns=ncolumns,
        dt=dt,end=1*dt,Tdump=2*dt,
        Nsq=Nsq,alpha=alpha,beta=beta,f=f,
        initial_conditions = {"b": b0_exp, "p": "hydrostatic", "v":"geostrophic"},
        ksp_monitor_true_residual=False,
        incompressible=True,
        diagnostics = {"geostrophic_imbalance":[]},
        dirname='tests/linear_eady_incompressible')

    Model = LinearEadySlice(setup)
    Data, dirs = Model.run()

    ene_list = Data["total_energy"]

    return ene_list


def run_total_energy():
    
    ene_list = eady()

    # get initial and final total energy:
    ene_init = ene_list[0]
    ene_fin = ene_list[-1]
    
    # check energy of same order of magnitude as previous calculation
    assert abs(ene_init - 9.00843e10) < 1e6


def test_total_energy():
    run_total_energy()


# @pytest.mark.parallel(nprocs=2)
# def test_total_energy_parallel():
#     run_total_energy()

