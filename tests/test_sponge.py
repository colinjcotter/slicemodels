import pytest
import sys

from firedrake import *
from nonlinear_slice import NonlinearSliceConfiguration, NonlinearSlice

def sponge():

    # Parameters NH
    L = 144000.
    ncolumns = int(L/8000.)
    H = 35000.
    dz = 2500.
    nlayers = int(H/dz)
    dt = 5.
    Nsq = 1.0e-2**2

    ksp_atol = 1.e-04
    ksp_rtol = 1.e-04

    # Topography:
    topo_exp = Expression(("x[0]","x[1]+(H-x[1])*h*pow(a,2)/(H*(pow(x[0]-xc,2)+pow(a,2)))"), h=1., a=1000., H=H, xc=0.5*L)

    # Sponge:
    muexp = Expression("x[1] < zc ? 0.0 : mubar*pow(sin((pi/2.)*(x[1]-zc)/(1-zc)),2)", zc=H-10000., mubar=0.15/dt)

    # Initial conditions
    u0 = Expression(("10.0","0.0"))

    setup = NonlinearSliceConfiguration(
        L=L, H=H, nlayers=nlayers, ncolumns=ncolumns,
        dt=dt, end=2*dt, Tdump=10*dt, 
        Nsq=Nsq, topo_exp = topo_exp, mu_exp=muexp,
        initial_conditions = {"uw": u0},
        dirname='tests/mountain')
                                    

    Model = NonlinearSlice(setup)

    Data, dirs = Model.run()

    ene_list = Data["total_energy"]
    return ene_list, ksp_rtol

def run_total_energy():
    
    ene_list, ksp_rtol = sponge()

    # get initial and final total energy:
    ene_init = ene_list[0]
    ene_fin = ene_list[-1]
    
    # check energy of same order of magnitude as previous calculation
    assert abs(ene_init - 2.52e11) < 2e5
    # check energy doesn't change too much:
    assert abs(ene_fin - ene_init) < ksp_rtol*ene_init

def test_total_energy():
    run_total_energy()
