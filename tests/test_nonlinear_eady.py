import pytest
import sys
import inifns

from nonlinear_eady_slice import NonlinearEadySlice, NonlinearEadySliceConfiguration
from firedrake import *

def nonlinear_eady():

    L = 1000000.
    H = 10000.
    f = 1.0e-4
    a = -30.
    Nsq = 2.5e-05
    Bu = 0.5

    nlayers = 30
    ncolumns = 30
    dt = 100.

    alpha = 0.5
    beta = 1.0

    dt = beta*dt
    f = f/beta
    L = beta*L

    template_s = inifns.template_target_strings()
    b0_exp = Expression(template_s,a=a,Nsq=Nsq,Bu=Bu,H=H,L=L)

    setup = NonlinearEadySliceConfiguration(
        L=2*L,H=H,nlayers=nlayers,ncolumns=ncolumns,
        dt=dt,end=1*dt,Tdump=2*dt,
        Nsq=Nsq,alpha=alpha,beta=beta,f=f,
        initial_conditions = {"b": b0_exp, "p": "hydrostatic", "v":"geostrophic"},
        advection_options = {
            "uw":{"formulation":"vec_invariant_novort","scheme":"ssprk"}
        },
        incompressible=True,
        diagnostics = {"geostrophic_imbalance":[], "eady_potential_energy":[], "eady_total_energy":[]},
        diagnostics_to_print = ["kinetic_energy_u", "kinetic_energy_v", "eady_potential_energy", "eady_total_energy", "geostrophic_imbalance", "courant_number_x", "courant_number_z"],
        ksp_monitor_true_residual = False,
        dirname='tests/nonlinear_eady_incompressible')

    Model = NonlinearEadySlice(setup)
    Data, dirs = Model.run()

    return Data


def run_diagnostics():
    
    Data = nonlinear_eady()

    # results from previous simulation
    old = {}
    old["eady_total_energy"] = 101609323569.94357
    old["geostrophic_imbalance"] = 0.0022208029808559455
    old["courant_number_x"] = 0.0077494569252239054
    old["courant_number_z"] = 0.00015364601691381365

    diagnostics = ["eady_total_energy", "geostrophic_imbalance", "courant_number_x", "courant_number_z"]
    for d in diagnostics:
        assert abs(Data[d][-1] - old[d]) < 5.e-4*old[d]

    old_rmsv = 1.3508902175360864
    old_vmax = 3.3357699000790437
    rmsv = Data["v"]["rms"][-1]
    vmax = Data["v"]["max"][-1]
    assert abs(rmsv - old_rmsv) < 0.001*old_rmsv
    assert abs(vmax - old_vmax) < 0.001*old_vmax

def test_diagnostics():
    run_diagnostics()


# @pytest.mark.parallel(nprocs=2)
# def test_total_energy_parallel():
#     run_total_energy()

