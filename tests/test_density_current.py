import pytest
import sys

from firedrake import *
from nonlinear_slice import NonlinearSliceConfiguration, NonlinearSlice

def dc():

    # set parameters
    L = 51200.
    H = 6400.
    nlayers = int(H/400.)
    ncolumns = int(L/400.)
    dt = 1.
    ksp_atol = 1.e-04
    ksp_rtol = 1.e-04

    # set up initial conditions
    b0 = Expression("sqrt(pow((x[0]-xc)/xr,2)+pow((x[1]-zc)/zr,2)) > 1. ? 0.0 : g*15/(2.*bartheta)*(cos(pi*(sqrt(pow((x[0]-xc)/xr,2)+pow((x[1]-zc)/zr,2))))+1)", xc=L/2., xr=4000., zc=3000., zr=2000., g=-9.81, bartheta=300.)

    setup = NonlinearSliceConfiguration(
        L=L, H=H, nlayers=nlayers, ncolumns=ncolumns,
        output_fields=['b'], dt=dt, end=dt, Tdump=dt, 
        initial_conditions = {"b" : b0},
        periodic=False,
        advection_options = {"uw": {"formulation": "vec_invariant",
                                    "diffusion": { "scheme": "cg",
                                                   "kappa": 75.}},
                             "b": {"diffusion": { "scheme": "interior_penulty",
                                                  "kappa": 75.}}},
        ksp_atol=ksp_atol, ksp_rtol=ksp_rtol, ksp_monitor_true_residual=False,
        dirname='tests/test_density_current')

    Model = NonlinearSlice(setup)

    Data, dirs = Model.run()

    # check kinetic energy as Nsq=0.
    ene_list = Data["kinetic_energy"]
    print ene_list
    return ene_list, ksp_rtol


def run_total_energy():
    
    ene_list, ksp_rtol = dc()

    # get initial and final total energy:
    ene_init = ene_list[0]
    ene_fin = ene_list[-1]

    # not much happening at the start - kinetic energy is zero!
    # check energy of same order of magnitude as previous calculation
    assert abs(ene_fin - 5.018e5) < 1e2


def test_total_energy():
    run_total_energy()
