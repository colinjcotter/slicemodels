import pytest
from firedrake import *
from nonlinear_slice import NonlinearSliceConfiguration, NonlinearSlice, EtaAdvection, PAdvection, BAdvection
from utility_functions import make_fbar, make_supg_test_function
import numpy as np

class TestEtaAdvection(EtaAdvection):

    def setup_eta_solver(self, tracer_exp):
        self.oldf.project(tracer_exp)

class Advection(NonlinearSlice):

    def __init__(self, setup, function_space):

        NonlinearSlice.__init__(self, setup)
        self.function_space = function_space

    def run(self):
        p = self.parameters
        oldu, oldv, oldb, oldp = self.old.vars.split()
        newu, newv, newb, newp = self.new.vars.split()

        if self.function_space is "CG":
            test = TestEtaAdvection(p, self.old.eta, self.new.eta, 
                                    self.old, self.new,
                                    p.advection_options["u"], 
                                    self.no_normal_flow_bc_ids)
            test.setup_eta_solver(p.initial_conditions["eta"])
#            self.new.eta.assign(self.old.eta)
        elif self.function_space is "DG":
            test = PAdvection(p, oldp, newp, 
                              self.old, self.new,
                              p.advection_options["p"])
            test.newf = test.fstar
        elif self.function_space is "Vt":
            test = BAdvection(p, oldb, newb, 
                              self.old, self.new,
                              p.advection_options["b"])
            test.newf = test.fstar
        else:
            print "Invalid choice of function space"

        test.setup_solver()
        test.setup_diffusion()

        test.newf.assign(test.oldf)
        self.dump()

        for nstep in range(self.nsteps+1):
            p.initial_conditions["u"].t = p.t
            oldu.project(p.initial_conditions["u"])
            test.advection()
            test.diffusion()
            test.oldf.assign(test.newf)
            print nstep, self.nsteps, p.t, p.dt, p.Tdump, p.t%p.Tdump
            p.t += p.dt
            if ((p.t%p.Tdump) < p.dt):
                self.dump()


@pytest.mark.skipif("config.option.short")
def test_adv_diff():
    import vtk

    space_field_dict = {"CG":"eta","DG":"p","Vt":"b"}
    
    tracer1 = Expression(("sin(2*pi*x[0])*sin(2*pi*x[1])"))
    u1 = Expression(("1.0","0.0"))

    tracer2 = Expression(("1 < 4*sqrt(pow(x[0]-0.25,2)+pow(x[1]-0.25,2)) ? 0.5*(1+cos(pi)) : 0.5*(1+cos(4*pi*sqrt(pow(x[0]-0.25,2)+pow(x[1]-0.25,2))))"))
    u2 = Expression(("pow(sin(pi*x[0]),2)*sin(2*pi*x[1])*cos(pi*t/4.)","-pow(sin(pi*x[1]),2)*sin(2*pi*x[0])*cos(pi*t/4.)"), t=0.0)

    ics = {"const_adv":{"tracer_exp":tracer1, "u_exp":u1},
           "swirling":{"tracer_exp":tracer2, "u_exp":u2}}

    ncols = int(1/0.01)
    dt = 0.005


    for setup_name, ic in ics.iteritems():
        print setup_name

        for space, field in space_field_dict.iteritems():

            filename = space+"_"+setup_name

            setup = NonlinearSliceConfiguration(
                L=1., H=1., nlayers=ncols, ncolumns=ncols,
                csq=0., dt=dt, end=4.0, Tdump=4.0,
                output_fields=[field],
                initial_conditions = {field: ic["tracer_exp"],
                                  "u": ic["u_exp"]},
                periodic = True,
                advection_options = {
                    "u":{"formulation":"vec_invariant"}},
                filename=filename)

            Model = Advection(setup, space)
            Model.run()
        
            # setup vtk stuff:
            gridreader = vtk.vtkXMLUnstructuredGridReader()
            data = [[],[]]

            for i in range(2):
                f = filename+"_"+field+"_"+str(i)+".vtu"
                gridreader.SetFileName(f)
                gridreader.Update()
                ugrid = gridreader.GetOutput()
                pointdata = ugrid.GetPointData()
                fieldname = pointdata.GetArrayName(0)
                vtkdata = pointdata.GetScalars(fieldname)
                data[i] = np.array(
                    [vtkdata.GetTuple1(j) for j in range(
                            vtkdata.GetNumberOfTuples())])

            # find magnitude of difference:
            print "Difference:", filename, np.abs(data[1]-data[0]).max()
