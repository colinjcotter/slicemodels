from diagnostics import *
from dummy_diagnostics import *

def geostrophic_imbalance(p, geo):
    V = FunctionSpace(geo.function_space().mesh(), "DG", 1)
    c = Function(V)
    c.assign(1)
    pg = sqrt(assemble((geo[0]/p.f)**2*dx)/assemble(c*dx))
    return pg

def eady_potential_energy(p, old):
    oldu, oldv, oldb, oldp = old.vars.split()
    V2 = oldp.function_space()
    Forcing = Function(V2).interpolate(Expression(("x[1]-H/2"),H=p.H))
    potential = assemble(-Forcing*oldb*dx)
    return potential

def eady_total_energy(p, old):
    total = (
        kinetic_energy(p, old) + eady_potential_energy(p, old) 
    )
    return total

def true_residual_v(p, vtres):
    vtres_max = max(vtres)
    vtres_min = min(vtres)
    if abs(vtres_max) < abs(vtres_min):
        max_amplitude = abs(vtres_min)
    else:
        max_amplitude = abs(vtres_max)
    return max_amplitude
