\documentclass{article}
\usepackage[nosumlimits]{amsmath}
\usepackage{amssymb,amsthm,MnSymbol}
\def\MM#1{\boldsymbol{#1}}
\usepackage{color}
\newcommand{\pp}[2]{\frac{\partial #1}{\partial #2}} 
\newcommand{\dede}[2]{\frac{\delta #1}{\delta #2}}
\newcommand{\dd}[2]{\frac{\diff#1}{\diff#2}}
\newcommand{\dt}{\Delta t}
\def\MM#1{\boldsymbol{#1}}
\DeclareMathOperator{\diff}{d}
\DeclareMathOperator{\DIV}{DIV}
\DeclareMathOperator{\D}{D}
\usepackage{amscd}
\usepackage{natbib}
\bibliographystyle{elsarticle-harv}
\usepackage{helvet}
\usepackage{amsfonts}
\renewcommand{\familydefault}{\sfdefault} %% Only if the base font of the docume
\newcommand{\vecx}[1]{\MM{#1}}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{lemma}[theorem]{Lemma}
\newcommand{\code}[1]{{\ttfamily #1}} 
\usepackage[margin=2cm]{geometry}

\usepackage{fancybox}
\begin{document}
\title{Notes on compressible Boussinesq vertical slice formulation}
\author{Jemma Shipton}
\maketitle

\section{Boussinesq equations}

The compressible Boussinesq equations in a vertical slice are

\begin{align}
\MM{u}_t + (\MM{u}\cdot\nabla)\MM{u} -fv\hat{\MM{x}} &= 
-\nabla p + b\hat{\MM{z}} \label{mom_eqn} \\
v_t +(\MM{u}\cdot\nabla)v + fu &= 0 \label{v_eqn}\\
p_t + (\MM{u}\cdot\nabla)p + c^2(\nabla\cdot\MM{u}) &= 0 \label{pressure_eqn}\\
b_t + (\MM{u}\cdot\nabla)b + N^2w &= 0, \label{buoyancy_eqn}
\end{align}

\noindent where $\MM{u}=(u,w)$ is the in-slice velocity, $v$ is the
out-of-slice velocity, $p$ is the pressure,
$b=\frac{g}{\bar{\theta}}\theta '$ is the perturbation buoyancy which
is a function of the gravitational acceleration $g$, the potential
temperature perturbation $\theta '$ and the reference potential
temperature $\bar{\theta}$, $f$ is the Coriolis parameter, $c$ is the
speed of sound and $N=\frac{\partial\theta}{\partial z}$ is the
buoyancy frequency. 

To obtain the vorticity conserving formulation, we start with the
momentum equations \ref{mom_eqn}-\ref{v_eqn} in vector invariant
form. Initially we shall just apply this to the in-slice momentum
equation \ref{mom_eqn}:

\begin{equation}\label{vec_invariant_mom}
\MM{u}_t - \eta\MM{u}^\perp = -\nabla\left(p + \frac{1}{2}|\MM{u}|^2\right) + b\hat{\MM{z}} - fv\hat{\MM{x}},
\end{equation}

\noindent where $\eta = u_z - w_x = -\nabla^\perp\cdot\MM{u}$ and
$\nabla^\perp=(-\partial_z,\partial_x)$, and we will use a standard
scheme for $v$ (see later).

Applying $-\nabla^\perp\cdot$ to equation \ref{vec_invariant_mom}
gives an advection equation for the out-of-slice vorticity $\eta$:

\begin{equation}\label{eta_adv}
\eta_t + \nabla\cdot(\MM{u}\eta) = -b_x + fv_z.
\end{equation}

\noindent We use finite element spaces with the following properties:

\begin{equation}
 \begin{CD}
 V_0(\Omega) @> \nabla^\perp >> V_1(\Omega) @>\nabla\cdot >> V_2(\Omega),
 \end{CD}
\end{equation}

\noindent where $V_1$ has the horizontal/vertical splitting

\begin{equation}
V_1 = V_1^v\oplus V_1^h
\end{equation}

\noindent We shall also use a scalar space $V_b$ that has the same DOFs as $V_1^v$. We choose
\[
\eta\in V_0, \quad \MM{u}\in V_1, \quad b \in V_b, \quad p, v \in V_2.
\]

\noindent and require that $V_2$ is discontinuous so that we can use
DG advection methods for the pressure equation \ref{pressure_eqn}.

The finite element discretisation is then:

\begin{align}
\left<\MM{w},\MM{u}_t\right> + \left<\MM{w},\MM{Q}^\perp\right> + \left<\MM{w},\nabla\left(p + \frac{1}{2}|\MM{u}|^2\right)\right> - \left<\MM{w},b\hat{\MM{z}}\right> &= 0 \quad\forall\MM{w}\in V_1\\
\left<\phi,p_t\right> + \left<\phi,\MM{u}\cdot\nabla p\right> + c^2\left<\phi,\nabla\cdot\MM{u}\right> &= 0 \quad\forall\phi\in V_2\\
\left<\gamma,b_t\right> + \left<\gamma,\MM{u}\cdot\nabla b\right> + N^2\left<\gamma,\MM{u}\cdot\hat{\MM{z}}\right> &=0 \quad\forall\gamma\in V_b,\\
\left<\sigma,\eta\right> &= \left<\nabla^\perp\sigma,\MM{u}\right> \quad\forall\sigma\in V_0 \label{vorticity}
\end{align}

\noindent where we deduce $\MM{Q}^\perp$ from the discretisation of
the advection equation for $\eta$:

\begin{equation}\label{eta_adv_weak}
\left<\sigma,\eta_t\right> + \left<\sigma,\nabla\cdot(\MM{u}\eta)\right> = \left<\sigma,-b_x + fv_z\right> \quad\forall\sigma\in V_0.
\end{equation}

Discretising in time using the theta-method, we have

\begin{align}
\left<\MM{w}, \left(\MM{u}^{n+1} - \MM{u}^n\right)\right> + \dt \left<\MM{w}, \MM{Q}^\perp \right> - \dt \left<\nabla\cdot\MM{w}, \left(p^* + \frac{1}{2}\left|\MM{u}^*\right|\right)\right> - \dt \left<\MM{w}, b^*\hat{\MM{z}}\right> &= 0\\
\left<\phi, \left(p^{n+1} - p^n\right)\right> + \dt \left<\phi,\MM{u}^*\cdot\nabla p^*\right> + \dt c^2\left<\phi,\nabla\cdot\MM{u}^*\right> &= 0 \\
\left<\gamma, \left(b^{n+1} - b^n\right)\right> + \dt\left<\gamma, \MM{u}^*\cdot\nabla b^*\right> + \dt N^2\left<\gamma, \MM{u}^*\cdot\hat{\MM{z}}\right> &=0
\end{align}

\noindent where the star denotes $y^* = (1-\theta)y^n + \theta
y^{n+1}$ and we have integrated the pressure gradient term by
parts. We solve iteratively using a quasi Newton method based on the
Jacobian from the linear equations.


We now need to choose advection schemes for $p$ and $b$. Initially we
will use the theta-method with DG upwinding for $p$. Within each
element $e$, equation \ref{pressure_eqn} becomes, after integrating
the advection term by parts,

\begin{equation}
\int_e\phi\left(p^{n+1} - p^n\right) 
- \dt\int_e\nabla\cdot(\phi\bar{\MM{u}})p^*\diff\! x 
+ \dt c^2\int_e\phi\nabla\cdot\bar{\MM{u}}\diff\! x 
+ \dt\int_{\delta e}\phi\bar{\MM{u}}\cdot\MM{n}\tilde{p}\diff\! s = 0,
\end{equation}

\noindent where $\delta e$ denotes the facets of element $e$,
$\bar{\MM{u}}$ is the current value of $\MM{u}$ and $\tilde{p}$
denotes the upwind value of $p^*$. Summing over all elements gives

\begin{equation}
\int_\Omega\phi\left(p^{n+1} - p^n\right) 
- \dt \int_\Omega\nabla\cdot(\phi\bar{\MM{u}})p^*\diff\! x 
+ \dt c^2\int_\Omega\phi\nabla\cdot\bar{\MM{u}}\diff\! x 
+ \dt\int_{\Gamma}[\phi\bar{\MM{u}}]\tilde{p}\diff\! s = 0,
\end{equation}

\noindent where $\Gamma$ is the union of all element facets and $[.]$
denotes the jump across the facet.

The finite element space $V_b$ is the tensor product of a discrete
finite element space in the vertical and a continuous finite element
space in the horizontal so we use DG upwinding in the horizontal
direction and SUPG in the vertical. First we integrate the advection
term by parts twice (so as not to have a derivative of the test
function as this would interfere with the SUPG step), and then we
replace the test function $\gamma$ with $\gamma + \bar{w}\tau\gamma_z
$ where $\bar{w}$ is the vertical component of the velocity and
$\tau=\alpha/(\epsilon+|\bar{w}|)$. Integrating by parts once gives,
in each column,

\begin{equation}
\int_{col}\gamma\left(b^{n+1} - b^n\right)\diff\! x 
- \dt\int_{col}\nabla\cdot(\gamma\bar{\MM{u}})b^*\diff\! x 
+ \dt N^2\int_{col}\gamma\bar{w}\diff\! x 
+ \dt\int_{\Gamma_V}\gamma\bar{\MM{u}}\cdot\MM{n}\tilde{b}^* \diff\! s = 0.
\end{equation}

\noindent where $\Gamma_V$ denotes the vertical facets of each column
and $\tilde{b}$ denotes the upwind value of $b$. Integrating by parts
again gives

\begin{equation}
\int_{col}\gamma\left(b^{n+1} - b^n\right)\diff\! x 
+ \dt\int_{col}\gamma\bar{\MM{u}}\cdot\nabla b^*\diff\! x 
+ \dt N^2\int_{col}\gamma\bar{w}\diff\! x 
+ \dt\int_{\Gamma_V}\gamma\bar{\MM{u}}\cdot\MM{n}\tilde{b}^* \diff\! s 
- \dt\int_{\Gamma_V}\gamma\bar{\MM{u}}\cdot\MM{n}b_{int}^*\diff\! s =0.
\end{equation}

\noindent where the new boundary term contains $b_{int}$, the value of
$b$ on the interior of the column. Now, summing over the whole domain, we have

\begin{equation}
\int_\Omega\gamma\left(b^{n+1} - b^n\right)\diff\! x 
+ \dt\int_\Omega\gamma\bar{\MM{u}}\cdot\nabla b^*\diff\! x 
+ \dt N^2\int_\Omega\gamma\bar{w}\diff\! x 
+ \dt\int_{\Gamma_V}[\gamma\bar{\MM{u}}]b^* \diff\! s 
- \dt\int_{\Gamma_V}\gamma\bar{\MM{u}}\cdot\MM{n}\tilde{b}^*\diff\! s =0.
\end{equation}

\noindent Now applying SUPG in the vertical gives

\begin{multline}
\int_{col}(\gamma + \bar{w}\tau\gamma_z)\left(b^{n+1} - b^n\right)\diff\! x 
+ \dt\int_{col}(\gamma + \bar{w}\tau\gamma_z)\bar{\MM{u}}\cdot\nabla b^*\diff\! x 
+ \dt N^2\int_{col}(\gamma + \bar{w}\tau\gamma_z)\bar{w}\diff\! x \\
+ \dt\int_{\Gamma_V}(\gamma + \bar{w}\tau\gamma_z)\bar{\MM{u}}\cdot\MM{n}\tilde{b}^* \diff\! s 
- \dt\int_{\Gamma_V}(\gamma + \bar{w}\tau\gamma_z)\bar{\MM{u}}\cdot\MM{n}b\diff\! s =0.
\end{multline}

We now deduce $\MM{Q}^\perp$ from the advection scheme used for
$\zeta$. For example, if $V_0 = Q2$ (continuous, quadratic basis
functions) and we discretise equation \ref{zeta_adv_weak} using implicit
timestepping with SUPG, we replace the test function $\sigma$ with an
upwinded test function $\sigma +
\bar{\MM{u}}\cdot\MM{\tau}\cdot\nabla\sigma$ where

\begin{equation}
\MM{\tau} = \frac{1}{\epsilon + |\bar{\MM{u}}|} 
\begin{pmatrix}
\alpha_1\Delta x & 0 \\
0 & \alpha_2\Delta z
\end{pmatrix}
\end{equation}

\noindent Equation \ref{zeta_adv_weak} then becomes

\begin{equation}
\left<\sigma + \bar{\MM{u}}\cdot\MM{\tau}\cdot\nabla\sigma, (\zeta^{n+1}-\zeta^n) + \dt(\nabla\cdot(\bar{\MM{u}}\zeta^*) + \nabla \bar{b}\cdot\hat{\MM{x}})\right> = 0,
\end{equation}

\noindent where an overbar denotes the current value of that
variable. We rewrite this as

\begin{equation}
\left<\sigma, (\zeta^{n+1} - \zeta^n)\right> + \left<\nabla\sigma, \bar{\MM{u}}\cdot\MM{\tau} (\zeta^{n+1}-\zeta^n) \right> + \dt\left<\nabla\sigma,\bar{\MM{u}}\cdot\MM{\tau}\cdot(\nabla\cdot(\bar{\MM{u}}\zeta^*) + \nabla \bar{b}\cdot\hat{\MM{x}}) - (\bar{\MM{u}}\zeta^* + \bar{b}\hat{\MM{x}})\right>
\end{equation}

\noindent and substitute from equation \ref{vorticity} to obtain

\begin{equation}
  \left<\nabla^\perp\sigma, \MM{u}^{n+1}-\MM{u}^n\right> 
+ \left<\nabla^\perp\sigma, 
        (\bar{\MM{u}}\cdot\MM{\tau}(\zeta^{n+1}-\zeta^n))^\perp\right> 
+ \dt\left<\nabla^\perp\sigma, 
        (\bar{\MM{u}}\cdot\MM{\tau}\cdot
              (\bar{\MM{u}}\zeta^* + \nabla \bar{b}\cdot\hat{\MM{x}}) 
      - (\bar{\MM{u}}\zeta^* + \bar{b}\hat{\MM{x}}))^\perp\right>
\end{equation}

\noindent Since $\nabla^\perp\sigma\in V_1$ we can compare this with
equation \ref{vec_invariant_mom} to see that

\begin{equation}
\MM{Q} = - \bar{\MM{u}}\zeta^* + \bar{\MM{u}}\cdot\MM{\tau}\left(\frac{1}{\dt}(\zeta^{n+1} - \zeta) + \nabla\cdot(\bar{\MM{u}}\zeta^*) + \nabla \bar{b}\cdot\hat{\MM{x}}\right).
\end{equation}

We can now solve equations \ref{} using a quasi Newton method, based
on the Jacobian from the linearised equations.

\end{document}
