from nonlinear_slice import NonlinearSlice, NonlinearSliceConfiguration, UAdvection, PAdvection
from nonlinear_eady_model import EadyVAdvection, EadyBAdvection
from eady_tools import *
from utility_functions import *
from firedrake import *
import sys
import numpy as np

class NonlinearEadySliceConfiguration(NonlinearSliceConfiguration):

    beta = None
    dbdy = -10./300.*3.0e-06

    def __init__(self, **kwargs):

        NonlinearSliceConfiguration.__init__(self, **kwargs)

class NonlinearEadySlice(NonlinearSlice):

    def __init__(self, setup):

        NonlinearSlice.__init__(self, setup)

        self.diagnostics_mod = "eady_diagnostics"

        p = self.parameters

        # set field_advection
        oldu, oldv, oldb, oldp = self.old.vars.split()
        newu, newv, newb, newp = self.new.vars.split()

        self.field_advection = {
            "uw": UAdvection(p, oldu, newu, self.old, self.new,
                            p.advection_options["uw"], self.uinit,
                            self.old.eta, self.new.eta,
                            self.no_normal_flow_bc_ids),
            "v": EadyVAdvection(p, oldv, newv, self.old, self.new,
                            p.advection_options["v"], self.uinit,
                            self.old.omega, self.new.omega,
                            self.no_normal_flow_bc_ids),
            "b": EadyBAdvection(p, oldb, newb, self.old, self.new,
                            p.advection_options["b"]),
            "p": PAdvection(p, oldp, newp, self.old, self.new,
                            p.advection_options["p"]),
            }

        self.ures = self.field_advection["uw"].residual
        self.vres = self.field_advection["v"].residual
        self.bres = self.field_advection["b"].residual
        self.pres = self.field_advection["p"].residual

    def hydrostatic_base_pressure(self):

        # get functions
        p = self.parameters
        oldu, oldv, oldb, oldp = self.old.vars.split()
        Vb = oldb.function_space()

        self.b_b = Function(Vb).interpolate(Expression(("(x[1]-H/2)*Nsq"),H=self.p.H, Nsq=self.p.Nsq))
        self.file_b_b = File("%s/b_b.pvd" % (p.dirs))
        self.file_b_b.write(self.b_b)

        ##Calculate hydrostatic pressure
        # get F
        v = TrialFunction(self.V1v)
        w = TestFunction(self.V1v)

        bcs = [DirichletBC(self.V1v, Expression(("0.", "0.")), "bottom")]

        a = inner(w,v)*dx
        L = w[1]*self.b_b*dx
        F = Function(self.V1v)

        solve(a == L, F, bcs=bcs)

        # define mixed function space
        V2 = oldp.function_space()
        WV = (self.V1v)*(V2)

        # get pprime
        v,pprime = TrialFunctions(WV)
        w,phi = TestFunctions(WV)

        bcs = [DirichletBC(WV[0], Expression(("0.", "0.")), "bottom")]

        a = (
            inner(w,v) + div(w)*pprime + div(v)*phi
            )*dx
        L = phi*div(F)*dx
        w1 = Function(WV)

        solver_parameters={'ksp_type':'gmres',
                           'pc_type': 'fieldsplit',
                           'pc_fieldsplit_type': 'schur',
                           'pc_fieldsplit_schur_fact_type': 'full',
                           'pc_fieldsplit_schur_precondition': 'selfp',
                           'fieldsplit_1_ksp_type': 'preonly',
                           'fieldsplit_1_pc_type': 'gamg',
                           'fieldsplit_1_mg_levels_pc_type': 'bjacobi',
                           'fieldsplit_1_mg_levels_sub_pc_type': 'ilu',
                           'fieldsplit_0_ksp_type': 'richardson',
                           'fieldsplit_0_ksp_max_it': 4,
                           'ksp_atol': 1.e-08,
                           'ksp_rtol': 1.e-08}

        solve(a == L, w1, bcs=bcs,
              solver_parameters=solver_parameters)

        v,pprime = w1.split()
        self.p_b = Function(V2).assign(project(pprime,V2))
        self.file_p_b = File("%s/p_b.pvd" % (p.dirs))
        self.file_p_b.write(self.p_b)

        # setup solver for p_total
        self.file_p_total = File("%s/p_total.pvd" % (p.dirs))

        tri = TrialFunction(V2)
        tes = TestFunction(V2)

        ax = inner(tes,tri)*dx
        Lx = inner(tes,self.p_b+oldp)*dx
        self.p_total = Function(V2)

        ptotalproblem = LinearVariationalProblem(ax,Lx,self.p_total)
        self.ptotalsolver = LinearVariationalSolver(ptotalproblem, 
                                                    solver_parameters={'ksp_type': 'gmres'})

        # setup solver for b_total
        self.file_b_total = File("%s/b_total.pvd" % (p.dirs))

        tri = TrialFunction(Vb)
        tes = TestFunction(Vb)

        ax = inner(tes,tri)*dx
        Lx = inner(tes,self.b_b+oldb)*dx
        self.b_total = Function(Vb)

        btotalproblem = LinearVariationalProblem(ax,Lx,self.b_total)
        self.btotalsolver = LinearVariationalSolver(btotalproblem, 
                                                    solver_parameters={'ksp_type': 'gmres'})


    def initialise(self):
        # get parameters
        p = self.parameters
        oldu, oldv, oldb, oldp = self.old.vars.split()

        self.hydrostatic_base_pressure()

        # balanced initialization
        if p.initial_conditions["p"] is "hydrostatic":
            self.hydrostatic_initial_pressure()
            self.pressure_boundary_correction()
        if p.initial_conditions["v"] is "geostrophic":
            self.geostrophic_initial_velocity()
            solve_balanced_velocity(self.parameters,self.old)

        # setup variables and solvers for output_fields
        if "vtres" in p.output_fields:
            V = FunctionSpace(oldp.function_space().mesh(), "DG", 0)
            self.vtres = Function(V)
            self.field_dict["vtres"] = self.vtres
            self.diagnostic_fields_solver_dict["vtres"] = setup_vtres_solver(self.parameters,self.old_b,self.old,
                                                                             self.field_dict["vtres"])

        # setup variables and solvers for diagnostics
        if "geostrophic_imbalance" in p.diagnostics.keys():
            self.field_dict["geo"] = Function(oldu.function_space())
            self.diagnostic_fields_solver_dict["geo"] = setup_geostrophic_imbalance_solver(self.parameters,self.old,
                                                                                           self.field_dict["geo"])
            p.diagnostics["geostrophic_imbalance"] = ["geostrophic_imbalance",
                                                      {"p": p, "geo": self.field_dict["geo"]}]
        if "eady_potential_energy" in p.diagnostics.keys():
            p.diagnostics["eady_potential_energy"] = ["eady_potential_energy", {"p": p, "old": self.old}] 
        if "eady_total_energy" in p.diagnostics.keys():
            p.diagnostics["eady_total_energy"] = ["eady_total_energy", {"p": p, "old": self.old}] 
        if "true_residual_v" in p.diagnostics.keys():
            p.diagnostics["true_residual_v"] = ["true_residual_v",
                                                {"p": p, "vtres": self.field_dict["vtres"]}]

    def print_diagnostics(self):
        # print selected diagnostics every dstep
        for field in self.diagnostics_to_print:
            self.mpiprint(field+" = {}".format(self.Data[field]))
        self.mpiprint("RMSV = {}".format(self.Data["v"]["rms"]))
        self.mpiprint("Vmax = {}".format(self.Data["v"]["max"]))

    def print_dump(self):        
        oldu, oldv, oldb, oldp = self.old.vars.split()

        self.ptotalsolver.solve()
        self.file_p_total.write(self.p_total)

        self.btotalsolver.solve()
        self.file_b_total.write(self.b_total)

        # print residuals
        self.mpiprint("ures = {}".format(self.Data["ures"]["max"][-1]))
        self.mpiprint("vres = {}".format(self.Data["vres"]["max"][-1]))
        self.mpiprint("bres = {}".format(self.Data["bres"]["max"][-1]))
        self.mpiprint("pres = {}".format(self.Data["pres"]["max"][-1]))

