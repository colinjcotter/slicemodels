from nonlinear_slice import NonlinearSlice, UAdvection, BAdvection, PAdvection
from nonlinear_dummy_model import DummyVAdvection
from utility_functions import *
from firedrake import *
import sys
import numpy as np
op2.init(log_level='WARNING')
from firedrake.petsc import PETSc

class NonlinearDummySlice(NonlinearSlice):

    def __init__(self, setup):

        NonlinearSlice.__init__(self, setup)

        self.diagnostics_mod = "dummy_diagnostics"
        
        p = self.parameters

        # dummy variables
        class old_d(object): pass
        self.old_d = old_d()
        old_d.vars = Function(self.W)

        class new_d(object): pass
        self.new_d = new_d()
        new_d.vars = Function(self.W)

        class old_b(object): pass
        self.old_b = old_b()
        old_b.vars = Function(self.W)

        self.Deltavars_d = Function(self.W)

        # initial conditions for dummy variables
        self.field_dict_d = {name: func for (name, func) in 
                             zip(("uw", "v", "b", "p"), old_d.vars.split())}
        for field in p.fields:
            if isinstance(p.initial_conditions[field], expression.Expression):
                self.field_dict_d[field].project(p.initial_conditions[field])

        # set field_advection_d
        oldu_d, oldv_d, oldb_d, oldp_d = old_d.vars.split()
        newu_d, newv_d, newb_d, newp_d = new_d.vars.split()

        self.field_advection_d = {
            "uw": UAdvection(p, oldu_d, newu_d, self.old_d, self.new_d,
                            p.advection_options["uw"], self.uinit,
                            self.old.eta, self.new.eta,
                            self.no_normal_flow_bc_ids),
            "v": DummyVAdvection(p, oldv_d, newv_d, self.old_d, self.new_d,
                            p.advection_options["v"], self.uinit,
                            self.old.omega, self.new.omega,
                            self.no_normal_flow_bc_ids),
            "b": BAdvection(p, oldb_d, newb_d, self.old_d, self.new_d,
                            p.advection_options["b"]),
            "p": PAdvection(p, oldp_d, newp_d, self.old_d, self.new_d,
                            p.advection_options["p"]),
            }

        self.ures_d = self.field_advection_d["uw"].residual
        self.vres_d = self.field_advection_d["v"].residual
        self.bres_d = self.field_advection_d["b"].residual
        self.pres_d = self.field_advection_d["p"].residual

    def initialise(self):
        # get parameters
        p = self.parameters
        # initialisation
        if p.initial_conditions["p"] is "hydrostatic":
            self.hydrostatic_initial_pressure()
        if p.initial_conditions["v"] is "geostrophic":
            self.geostrophic_initial_velocity()
        # setup a list for dummy energy
        self.kineticv_dummy_sum = []
        # setup variables and solvers for diagnostics
        if "kinetic_energy_v_dummy" in p.diagnostics.keys():
            p.diagnostics["kinetic_energy_v_dummy"] = ["kinetic_energy_v_dummy", {"p": p, "old_d": self.old_d}]
        if "kinetic_energy_v_dummy_sum" in p.diagnostics.keys():
            p.diagnostics["kinetic_energy_v_dummy_sum"] = ["kinetic_energy_v_dummy_sum", {"dummy_list": self.kineticv_dummy_sum}]

    def setup_duvbp_solver_d(self):
        # get parameters
        p = self.parameters
        # get space
        W = self.W
        # get functions
        oldu, oldv, oldb, oldp = self.old_d.vars.split()
        # trial and test functions
        du, dv, db, dp = TrialFunctions(W)
        w, wv, gamma, phi = TestFunctions(W)

        if p.incompressible:
            f = 0.
        else:
            f = p.f

        Equ = (
            inner(w,du) 
            - p.alpha*p.dt*f*w[0]*dv - p.alpha*p.dt*div(w)*dp
            - p.alpha*p.dt*w[1]*(db - p.mu*du[1])
            + inner(w,self.ures_d)
            + wv*dv + p.alpha*p.dt*f*wv*du[0] + inner(wv,self.vres_d)
            + gamma*(db + p.alpha*p.dt*p.Nsq*du[1]) + gamma*self.bres_d
            + phi*self.pres_d
            )*dx
        
        if p.incompressible:
            Equ += phi*div(du)*dx
        else:
            Equ += phi*(dp + p.alpha*p.dt*p.csq*div(du))*dx

        A = lhs(Equ)
        L = rhs(Equ)

        # Boundary conditions
        bc1 = [DirichletBC(W[0], Expression(("0." , "0.")), x)
               for x in ["top", "bottom"]]
        
        if p.incompressible:
            # Hdiv preconditioner
            Ap = (
                inner(w,du) + p.L*p.L*div(w)*div(du) +
                phi*dp/p.L/p.L + 
                gamma*db +# gamma.dx(1)*db.dx(1) + 
                wv*dv
                )*dx
            Duvbpproblem = LinearVariationalProblem(A,L,self.Deltavars_d,bcs=bc1,aP=Ap)
            sp = {'ksp_type':'gmres',
                  #'ksp_gmres_restart':80,
                  'ksp_monitor_true_residual': p.ksp_monitor_true_residual,
                  'pc_type':'fieldsplit',
                  'pc_fieldsplit_type':'additive',
                  'fieldsplit_0_pc_type':'lu',
                  'fieldsplit_1_pc_type':'lu',
                  'fieldsplit_2_pc_type':'lu',
                  'fieldsplit_3_pc_type':'lu',
                  'fieldsplit_0_ksp_type':'preonly',
                  'fieldsplit_1_ksp_type':'preonly',
                  'fieldsplit_2_ksp_type':'preonly',
                  'fieldsplit_3_ksp_type':'preonly',
                  'ksp_atol': p.ksp_atol,
                  'ksp_rtol': p.ksp_rtol}
            nullspace = MixedVectorSpaceBasis(W, 
                                              [W.sub(0),W.sub(1),W.sub(2), 
                                               VectorSpaceBasis(constant=True)])
            self.Duvbpsolver_d = LinearVariationalSolver(Duvbpproblem,
                                                       solver_parameters=sp,
                                                       nullspace=nullspace)
        else:
            Duvbpproblem = LinearVariationalProblem(A,L,self.Deltavars_d,bcs=bc1)
            sp = {'pc_type': 'python', 
                  'ksp_type':'gmres',
                  'ksp_monitor': p.ksp_monitor,
                  'snes_monitor': p.snes_monitor,
                  'ksp_monitor_true_residual': p.ksp_monitor_true_residual}
            self.Duvbpsolver_d = LinearVariationalSolver(Duvbpproblem,
                                                       solver_parameters=sp)
            pc = uvbpPC()
            pc.setup(W,self.ures.function_space(),self.pres.function_space(),self.bres.function_space(),p.dt,p.alpha,p.Nsq,p.csq,p.f,p.ksp_atol,p.ksp_rtol)
            self.Duvbpsolver_d.snes.ksp.pc.setPythonContext(pc)

    def run(self):        
        # get parameters
        p = self.parameters
        oldu, oldv, oldb, oldp = self.old.vars.split()
        newu, newv, newb, newp = self.new.vars.split()

        # initialisation
        self.initialise()

        # setup residuals
        # Subclasses may redefine these functions
        for field in p.fields:
            self.field_advection[field].setup_solver()
        for field in p.fields:
            self.field_advection_d[field].setup_solver()

        # setup solvers, these are always the same
        self.setup_duvbp_solver()
        self.setup_duvbp_solver_d()

        # set newvel,newp,newb
        self.new.vars.assign(self.old.vars)
        self.old_d.vars.assign(self.old.vars)
        self.new_d.vars.assign(self.old.vars)
        self.old_b.vars.assign(self.old.vars)

        # setup prsolvers
        self.setup_project_result()

        # dump intial flow results and diagnostics
        self.get_diagnostics()
        self.dump()

        # time loop
        self.mpiprint("Starting the time loop!")

        with PETSc.Log().Stage('Time loop'):
            for nstep in range(self.nsteps):
                with PETSc.Log().Event('Timestep'):
                    for its in range(p.nits):
                        for field in p.fields:
                            self.field_advection[field].get_residual()
                            self.field_advection_d[field].get_residual()

                        #This code is always the same
                        self.Duvbpsolver.solve()
                        self.new.vars += self.Deltavars
                        self.Duvbpsolver_d.solve()
                        self.new_d.vars += self.Deltavars_d

                    self.old_b.vars.assign(self.old.vars)
                    self.old.vars.assign(self.new.vars)
                    self.old_d.vars.assign(self.new_d.vars)

                    # print current time
                    p.t += p.dt
                    self.mpiprint("{:0.1f} {:0.1f} {:0.1f}".format(p.t,p.end,p.Tdump))

                    # calculate dummy energy
                    oldu_b, oldv_b, oldb_b, oldp_b = self.old_b.vars.split()
                    oldu_d, oldv_d, oldb_d, oldp_d = self.old_d.vars.split()

                    diff = assemble(0.5*(oldv_d*oldv_d-oldv_b*oldv_b)*dx)

                    if len(self.kineticv_dummy_sum) == 0:
                        self.kineticv_dummy_sum.append(diff)
                    else:
                        dummy_sum = self.kineticv_dummy_sum[-1] + diff
                        self.kineticv_dummy_sum.append(dummy_sum)

                    # get diagnostics every dstep
                    with PETSc.Log().Event('Diagnostics'):
                        if ((nstep%p.dstep) == p.dstep-1):
                            self.get_diagnostics()

                    # dump results every Tdump
                    with PETSc.Log().Event('IO'):
                        if ((p.t%p.Tdump) < p.dt):
                            self.dump()

                    self.old_d.vars.assign(self.new.vars)
                    self.new_d.vars.assign(self.new.vars)

        return self.Data, p.dirs
