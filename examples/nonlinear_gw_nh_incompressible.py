from firedrake import *
from nonlinear_slice import NonlinearSliceConfiguration, NonlinearSlice
import pickle

L = 300000.
H = 10000.

nlayers = 10
ncolumns = 300
dt = 6.
end = 3000.
Tdump = 60.
Ddump = 6.

dstep = int(Ddump/dt)

# set up initial conditions
u0 = Expression(("0.0","0.0"))
#u0 = Expression(("20.0","0.0"))
b0 = Expression("0.01*sin(pi*x[1]/10000.)/(1+pow(x[0]-xc,2)/pow(a,2))", a=5000., xc=0.5*L)

setup = NonlinearSliceConfiguration(
    L=L, H=H, nlayers=nlayers, ncolumns=ncolumns,
    dt=dt, end=end, Tdump=Tdump, dstep=dstep, nits=4, Nsq=1.e-4,
    initial_conditions = {"uw":u0,"b":b0},
    output_fields=['b','uw','v','p'], output_fields_FS={'u':'V0','w':'V0'},
    advection_options = {
        "uw":{"formulation":"vec_invariant_novort","scheme":"ssprk"}
    },
    incompressible=True,
    ksp_monitor_true_residual=False,
#    diagnostics_to_print = ["kinetic_energy_u", "kinetic_energy_v", 
#                            "potential_energy", "total_energy_incompressible"],
    dirname='gw_nh_incompressible')

Model = NonlinearSlice(setup)
data, dirs = Model.run()

pickle.dump(data, open("%s/%s.p" % (dirs, "data"), "wb"))
