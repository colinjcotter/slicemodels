from firedrake import *
from nonlinear_slice import NonlinearSliceConfiguration, NonlinearSlice

# Parameters NH
L = 240000.
ncolumns = int(L/4000.)
H = 50000.
dz = 500.
nlayers = int(H/dz)
dt = 20.
Nsq = 0.000383
print ncolumns, nlayers, 20*dt/4000.
# sponge
muexp = Expression("x[1] <= H - zd ? 0.0 : mubar*pow(sin((pi/2.)*(H-x[1]-zd)/zd),2)", H=H, zd=20000., mubar=0.3/dt)

# Topography:
topo_exp = Expression(("x[0]","x[1]+(H-x[1])*h*pow(a,2)/(H*(pow(x[0]-xc,2)+pow(a,2)))"), h=1., a=10000., H=H, xc=0.5*L)

# Initial conditions
u0 = Expression(("20.0","0.0"))

setup = NonlinearSliceConfiguration(
    L=L, H=H, nlayers=nlayers, ncolumns=ncolumns,
    dt=dt, end=15000, Tdump=20.*dt,
    output_fields=['b','p','v'], output_fields_FS = {'w':'V0'},
    Nsq=Nsq, topo_exp=topo_exp, mu_exp=muexp,
    initial_conditions = {"uw":u0},
    advection_options={
        "uw":{"formulation":"vec_invariant","scheme":"ssprk"}},
    dirname='mountain_h')

Model = NonlinearSlice(setup)


Model.run()
