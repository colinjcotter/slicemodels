from linear_eady_model import LinearEadySliceConfiguration, LinearEadySlice
from firedrake import *
import inifns
import pickle

L = 1000000.
H = 10000.
f = 1.0e-4
a = -30.
Nsq = 2.5e-05
Bu = 0.5

nlayers = 50
ncolumns = 50
dt = 100.
# end = 60*60.
# Tdump = 200.
# Ddump = 200.
end = 30*24*60*60.
Tdump = 12*60*60.
Ddump = 2*60*60. 

alpha = 0.5
beta = 1.0

dt = beta*dt
f = f/beta
L = beta*L
dstep = int(Ddump/dt)

# set up initial conditions  
template_s = inifns.template_target_strings()
b0_exp = Expression(template_s,a=a,Nsq=Nsq,Bu=Bu,H=H,L=L)

setup = LinearEadySliceConfiguration(
    L=2*L,H=H,nlayers=nlayers,ncolumns=ncolumns,
    dt=dt,end=end,Tdump=Tdump,dstep=dstep,
    Nsq=Nsq,alpha=alpha,beta=beta,f=f,
    initial_conditions = {"b": b0_exp, "p": "hydrostatic", "v":"geostrophic"},
    output_fields=['b','uw','v','p'], output_fields_FS={'u':'V0','w':'V0'}, 
    ksp_monitor_true_residual=False,
    incompressible=True,
    dirname='linear_eady_incompressible')

Model = LinearEadySlice(setup)
data, dirs = Model.run()
pickle.dump(data, open("%s/%s.p" % (dirs, "data"), "wb"))

