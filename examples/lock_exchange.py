from firedrake import *
from nonlinear_slice import NonlinearSlice, NonlinearSliceConfiguration

# set paramters
L = 32.
H = 2.
nlayers = 46
ncolumns = 384
dt = 0.04

# set up initial conditions
b0 = Expression("0.5 - 0.5*erf((x[0]-xc)*33.4370152488211)", xc=L/2.)

setup = NonlinearSliceConfiguration(
    L=L, H=H, nlayers=nlayers, ncolumns=ncolumns,
    dt=dt,end=20, Tdump=dt,
    initial_conditions = {"b":b0},
    periodic=False,
    dirname='lock_exchange')

Model = NonlinearSlice(setup)

Model.run()
