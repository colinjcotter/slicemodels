from firedrake import *
from nonlinear_slice import NonlinearSliceConfiguration, NonlinearSlice

# Parameters NH
L = 100000.
ncolumns = int(L/500.)
H = 30000.
dz = 300.
nlayers = int(H/dz)
print ncolumns, nlayers
dt = 8.
Nsq = 1.0e-2**2
muexp = Expression("x[1] < zc ? 0.0 : mubar*pow(sin((pi/2.)*(x[1]-zc)/(1-zc)),2)", zc=H-10000., mubar=1.2/dt)

# Topography:
topo_exp = Expression(("x[0]","x[1]+(H-x[1])*h*exp(-pow((x[0]-xc)/a,2))*pow(cos(pi*(x[0]-xc)/ll),2)/H"), h=250., ll=4000., a=5000., xc=0.5*L, H=H)

# Initial conditions
u0 = Expression(("10.0","0.0"))

setup = NonlinearSliceConfiguration(
    L=L, H=H, nlayers=nlayers, ncolumns=ncolumns,
    output_fields = ['b'], output_fields_FS = {'w':'V0'},
    initial_conditions = {"uw":u0},
    dt=dt, end=0, Tdump=dt,
    Nsq=Nsq, topo_exp=topo_exp,
    dirname='schar')
                                    

Model = NonlinearSlice(setup)


Model.run()
