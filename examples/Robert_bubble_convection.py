from firedrake import *
from nonlinear_slice import NonlinearSliceConfiguration, NonlinearSlice

L = 1000.
H = 1000.
dx = 5.
ncolumns = int(L/dx)
nlayers = int(H/dx)
dt = 2.5

# set up initial conditions
b1 = Expression("sqrt(pow((x[0]-x1),2)+pow((x[1]-z1),2)) < a1 ? A1 : (g*A1/bartheta)*exp(-pow((sqrt(pow((x[0]-x1),2)+pow((x[1]-z1),2))-a1)/s1,2))", x1=500, z1=300, a1=150, A1=0.5, s1=50, g=-9.81, bartheta=30.)

b2 = Expression("sqrt(pow((x[0]-x2),2)+pow((x[1]-z2),2)) < a2 ? A2 : (g*A2/bartheta)*exp(-pow((sqrt(pow((x[0]-x2),2)+pow((x[1]-z2),2))-a2)/s2,2))", x2=560, z2=640, a2=0, A2=-0.15, s2=50, g=-9.81, bartheta=30.)

b0exp = Expression("A1*exp(-pow((sqrt(pow((x[0]-x1),2)+pow((x[1]-z1),2))-a1)/s1,2)) + A2*exp(-pow((sqrt(pow((x[0]-x2),2)+pow((x[1]-z2),2))-a2)/s2,2))", x1=500, z1=300, a1=150, A1=0.5, s1=50, x2=560, z2=640, a2=0, A2=-0.15, s2=50, g=-9.81, bartheta=30.)

setup = NonlinearSliceConfiguration(
    L=L,H=H,nlayers=nlayers,ncolumns=ncolumns,
    dt=dt, end=10*60, Tdump=dt,
    initial_conditions = {"b":b0exp},
    output_fields=['b'], output_fields_FS={'b':'V0'},
    dirname='double_bubble')


Model = NonlinearSlice(setup)

Model.run()
