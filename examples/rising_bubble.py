from firedrake import *
from nonlinear_slice import NonlinearSliceConfiguration, NonlinearSlice

# set paramters
L = 1000.
H = 1000.
nlayers = int(H/10.)
ncolumns = int(L/10.)
dt = 1.

# set up initial conditions
b0 = Expression("sqrt(pow(x[0]-xc,2)+pow(x[1]-zc,2)) > rc ? 0.0 : g*0.5/(2.*bartheta)*(1. + cos((pi/rc)*(sqrt(pow((x[0]-xc),2)+pow((x[1]-zc),2)))))", xc=500., zc=350., rc = 250., g=9.81, bartheta=300.)

setup = NonlinearSliceConfiguration(
    L=L, H=H, nlayers=nlayers, ncolumns=ncolumns, 
    output_fields = ['b'],
    dt=dt,end=700, Tdump=10., 
    initial_conditions = {"b":b0},
    periodic=False, 
    advection_options = {
        "uw":{"formulation":"vec_invariant"}},
    dirname='rising_bubble')

Model = NonlinearSlice(setup)

Model.run()
