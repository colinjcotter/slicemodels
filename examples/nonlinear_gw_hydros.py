from firedrake import *
from nonlinear_slice import NonlinearSlice, NonlinearSliceConfiguration

# set parameters
L = 6000000.
H = 10000.
nlayers = 10
ncolumns = 300
dt = 100.
Nsq = 1.e-2**2

# set up initial conditions
u0 = Expression(("20.0","0.0"))
b0 = Expression("0.01*sin(pi*x[1]/10000.)/(1+pow(x[0]-xc,2)/pow(a,2))", a=100000., xc=0.5*L)

setup = NonlinearSliceConfiguration(
    L=L, H=H, nlayers=nlayers, ncolumns=ncolumns,
    dt=dt, end=60000, Tdump=10*dt,
    output_fields=['b','v'], output_fields_FS={'b':'V0'},
    Nsq=Nsq, f=1.e-4,
    initial_conditions = {"uw":u0, "b":b0, "p":"hydrostatic"},
    advection_options = {
        "uw":{"formulation":"vec_invariant","scheme":"ssprk"}},
    dirname='gw_h')

Model = NonlinearSlice(setup)

Model.run()
