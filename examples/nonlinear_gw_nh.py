from firedrake import *
from nonlinear_slice import NonlinearSliceConfiguration, NonlinearSlice

dt = 6.

# set up initial conditions
u0 = Expression(("20.0","0.0"))
b0 = Expression("0.01*sin(pi*x[1]/10000.)/(1+pow(x[0]-100000.,2)/pow(5000.,2))")

setup = NonlinearSliceConfiguration(
    L=300000., H=10000., nlayers=10, ncolumns=300,
    dt=dt, end=3000., Tdump=10*dt, nits=4, Nsq=1.e-4,
    output_fields=['b'], output_fields_FS={'b':'V0'},
    initial_conditions = {"uw":u0,"b":b0},
    advection_options = {
        "uw":{"formulation":"vec_invariant_novort","scheme":"ssprk"}},
    dirname='gw_nh')

Model = NonlinearSlice(setup)

Model.run()
