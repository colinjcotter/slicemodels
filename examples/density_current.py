from firedrake import *
import pickle
from nonlinear_slice import NonlinearSliceConfiguration, NonlinearSlice

# set paramters
L = 25600.
H = 6400.

g = 9.81
bartheta = 300.

res_dt = [(800.,4.),(400.,2.),(200.,1.),(100.,0.5),(50.,0.25)]

# set up initial conditions
b0 = Expression("sqrt(pow(x[0]/xr,2)+pow((x[1]-zc)/zr,2)) > 1. ? 0.0 : -g*15/(2.*bartheta)*(cos(pi*(sqrt(pow(x[0]/xr,2)+pow((x[1]-zc)/zr,2))))+1)", xr=4000., zc=3000., zr=2000., g=g, bartheta=bartheta)

kappa = 75.0
kappa_b = g*kappa/bartheta

for params in res_dt:
    delta_x, dt = params

    nlayers = int(H/delta_x)
    ncolumns = int(L/delta_x)
    dirname = 'density_current_dx'+str(delta_x)+'_dt_'+str(dt)

    setup = NonlinearSliceConfiguration(
        L=L, H=H, nlayers=nlayers, ncolumns=ncolumns,
        output_fields=['b'], output_fields_FS={'b':'V0'},
        dt=dt, end=80, Tdump=8., 
        initial_conditions = {"b":b0},
        periodic=False,
        advection_options = {
            "uw":{"formulation":"vec_invariant_novort","scheme":"ssprk",
                 "diffusion":{
                     "scheme":"interior_penulty","kappa":kappa}},
            "b":{"diffusion":{"scheme":"interior_penulty","kappa":kappa_b}}},
        dirname=dirname)

    Model = NonlinearSlice(setup)

    data, dirs = Model.run()
    pickle.dump(data, open("%s/%s.p" % (dirs, "data"), "wb"))
