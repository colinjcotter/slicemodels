from firedrake import *
from nonlinear_slice import NonlinearSliceConfiguration, NonlinearSlice

# Parameters NH
L = 144000.
ncolumns = int(L/800.)
H = 35000.
dz = 500.
nlayers = int(H/dz)
dt = 5.
Nsq = 1.0e-2**2

# sponge
mu_top = Expression("x[1] <= zc ? 0.0 : mubar*pow(sin((pi/2.)*(x[1]-zc)/(H-zc)),2)", H=H, zc=(H-10000.), mubar=0.15/dt)

# Topography:
topo_exp = Expression(("x[0]","x[1]+(H-x[1])*h*pow(a,2)/(H*(pow(x[0]-xc,2)+pow(a,2)))"), h=1., a=1000., H=H, xc=0.5*L)

# Initial conditions
u0 = Expression(("10.0","0.0"))

setup = NonlinearSliceConfiguration(
    L=L, H=H, nlayers=nlayers, ncolumns=ncolumns,
    dt=dt, end=9000, Tdump=10.*dt,
    initial_conditions = {"uw":u0},
    output_fields=['b','uw'], output_fields_FS = {'w':'V0'},
    Nsq=Nsq, topo_exp=topo_exp, 
    dirname='mountain_nh')

Model = NonlinearSlice(setup)

Model.run()
