clean:	cleantest
	rm -f *.vtu *.pvtu *.pvd *.pyc

cleantest:
	rm -f tests/*.vtu tests/*.pvtu tests/*.pvd tests/*.pyc
	rm -rf results/tests

test:	cleantest
	@echo "    Running all short tests"
	@py.test --short tests $(PYTEST_ARGS)

longtest:	cleantest
	@echo "    Running all tests"
	@py.test tests $(PYTEST_ARGS)
