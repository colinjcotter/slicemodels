from firedrake import *
import numpy as np

def make_fbar(alpha, old, new):
    fbar = (1 - alpha)*old + alpha*new
    return fbar

def make_supg_test_function(test, u, dt, supg_params=None):
    params = supg_params.copy() if supg_params else {}
    params.setdefault('a0', dt/sqrt(15.))
    params.setdefault('a1', dt/sqrt(15.))
    taux = params["a0"]
    tauz = params["a1"]
    tau = Constant(((taux,0.),(0.,tauz)))
    dtest = dot(dot(u, tau), grad(test))
    return test + dtest, dtest, tau

def make_vertical_supg_test_function(test, u, dt, supg_params=None):
    params = supg_params.copy() if supg_params else {}
    params.setdefault('a1', dt/sqrt(15.))
    tauz = params["a1"]
    dtest = tauz * u[1] * test.dx(1)
    return test + dtest

def make_projection(expr, target, solver_parameters=None):
    V = target.function_space()
    p = TestFunction(V)
    q = TrialFunction(V)
    a = inner(p, q) * dx
    L = inner(p, expr) * dx
 
    prob = LinearVariationalProblem(a, L, target)
    parameters = solver_parameters.copy() if solver_parameters else {}
    parameters.setdefault('ksp_type', 'cg')
    parameters.setdefault('ksp_rtol', 1e-8)
    solver = LinearVariationalSolver(prob, solver_parameters=parameters)
    return solver 

def projection_solver(input, output, outspace=None):

    if outspace is None:
        outspace = output.function_space()

    # trial and test functions
    tri = TrialFunction(outspace)
    tes = TestFunction(outspace)

    # setup a solver to project input onto outspace
    ax = inner(tes,tri)*dx
    Lx = inner(tes,input)*dx
    prproblem = LinearVariationalProblem(ax, Lx, output)
    prsolver = LinearVariationalSolver(prproblem, 
                                       solver_parameters={'ksp_type': 'cg'})
    return prsolver
