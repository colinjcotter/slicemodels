# README #

This is the repository for a vertical slice model built using compatible finite elements. To use it, you need to install Firedrake (https://firedrakeproject.org).

Repository owner: Colin Cotter (colin.cotter@imperial.ac.uk)