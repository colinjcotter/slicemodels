from firedrake import *

def setup_geostrophic_imbalance_solver(p,old,geo):
    oldu, oldv, oldb, oldp = old.vars.split()
    V1 = oldu.function_space()
    v = TrialFunction(V1)
    w = TestFunction(V1)
    a = inner(w,v)*dx
    L = (div(w)*oldp+inner(w,as_vector([p.f*oldv,oldb])))*dx
    bc1 = [DirichletBC(V1, Expression(("0." , "0.")), x)
           for x in ["top", "bottom"]]
    geoproblem = LinearVariationalProblem(a, L, geo, bcs=bc1)
    geosolver = LinearVariationalSolver(geoproblem,
                                        solver_parameters={'ksp_type': 'cg'})
    return geosolver 

def setup_vtres_solver(p,old,new,vtres):
    oldu, oldv, oldb, oldp = old.vars.split()
    newu, newv, newb, newp = new.vars.split()
    ubar = 0.5*(oldu+newu)
    vbar = 0.5*(oldv+newv)
    V = FunctionSpace(oldp.function_space().mesh(), "DG", 0)
    Forcing = Function(V).interpolate(Expression(("x[1]-H/2"),H=p.H))
    wv = TestFunction(V)
    v = TrialFunction(V)
    vlhs = wv*v*dx
    vrhs = wv*((newv-oldv)/p.dt + ubar[0]*vbar.dx(0) + ubar[1]*vbar.dx(1) 
               + p.f*ubar[0] + p.dbdy*Forcing)*dx
    vtresproblem = LinearVariationalProblem(vlhs, vrhs, vtres)
    vtressolver = LinearVariationalSolver(vtresproblem,
                                          solver_parameters={'ksp_type': 'cg'})
    return vtressolver

def solve_balanced_velocity(p,old):
    oldu, oldv, oldb, oldp = old.vars.split()

    ##Solve the blanced equation
    # psi is a stream function
    V0 = old.eta.function_space()
    xsi = TestFunction(V0)
    psi = TrialFunction(V0)
    Forcing = Function(V0).interpolate(Expression(("x[1]-H/2"),H=p.H))
    bc1 = [DirichletBC(V0, Expression("0."), x)
           for x in ["top", "bottom"]]
    Equ = (
        xsi.dx(0)*p.Nsq*psi.dx(0) + xsi.dx(1)*p.f**2*psi.dx(1)
        + p.dbdy*xsi.dx(0)*oldv - p.dbdy*xsi.dx(1)*p.f*Forcing
        )*dx
    Au = lhs(Equ)
    Lu = rhs(Equ)
    stm = Function(V0)
    solve(Au == Lu, stm, bcs=bc1)

    # get oldu
    V1 = oldu.function_space()
    utrial = TrialFunction(V1)
    w = TestFunction(V1)
    a = inner(w,utrial)*dx
    L = (w[0]*(-stm.dx(1))+w[1]*(stm.dx(0)))*dx
    solve(a == L, oldu)

