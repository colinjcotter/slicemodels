from slicemodels import LinearSlice, SliceConfiguration, FieldAdvection
from firedrake import *
from utility_functions import *
import numpy as np

class NonlinearSliceConfiguration(SliceConfiguration):

    default_advection_options = {
        "scheme": "ssprk", "supg": None}

    def __init__(self, **kwargs):

        self.advection_options = {}

        SliceConfiguration.__init__(self, **kwargs)

        for field in self.fields:
            self.advection_options.setdefault(field, {})

        for key, val in self.default_advection_options.iteritems():
            for field in self.fields:
                self.advection_options[field].setdefault(key, val)

        self.advection_options["uw"].setdefault("formulation",
                                                     "vec_invariant")
        self.advection_options["v"].setdefault("formulation",
                                                     "nonlinear_advection")
        assert all(f in self.fields  for f in self.advection_options.keys())

class NonlinearFieldAdvection(FieldAdvection):

    def __init__(self, p, oldf, newf, old, new, advection_options):

        FieldAdvection.__init__(self, p, oldf, newf, old, new)
        print "advection_options:", advection_options
        self.advection_options = advection_options
        self.n = FacetNormal(oldf.function_space().mesh())
        self.fstar = Function(self.function_space)

        if advection_options is not None:
            self.setup_solver = getattr(self,"setup_solver_"
                                        +advection_options["scheme"])
            self.advection = getattr(self,advection_options["scheme"])
            if advection_options["scheme"] is "ssprk":
                self.f1 = Function(self.function_space)
            if "diffusion" in advection_options:
                self.setup_diffusion = getattr(
                    self, "setup_diffusion_"+ 
                    advection_options["diffusion"]["scheme"])
                self.diffusion = getattr(
                    self, "diffusion_"+ 
                    advection_options["diffusion"]["scheme"])
                self.kappa = advection_options["diffusion"]["kappa"]

    def implicit(self):
        self.fstarsolver.solve()

    def ssprk(self):
        f1 = self.f1
        f1.assign(self.oldf)
        self.fstarsolver.solve()
        f1.assign(self.fstar)
        self.fstarsolver.solve()
        f1.assign(0.75*self.oldf + 0.25*self.fstar)
        self.fstarsolver.solve()
        self.fstar.assign((1.0/3.0)*self.oldf + (2.0/3.0)*self.fstar)

    def setup_diffusion(self):
        pass

    def diffusion(self):
        pass

    def get_residual(self):
        self.advection()
        self.diffusion()
        self.residual.assign(self.newf - self.fstar)

class BAdvection(NonlinearFieldAdvection):

    def setup_solver_implicit(self):
        dt = self.p.dt
        oldu, oldv, oldb, oldp = self.old.vars.split()
        newu, newv, newb, newp = self.new.vars.split()
        ubar = make_fbar(self.p.alpha, oldu, newu)
        n = self.n
        un = 0.5*(dot(ubar,n) + abs(dot(ubar,n)))
        bbar = (1-self.p.alpha)*self.oldf + self.p.alpha*self.trial
        gammaSU = make_vertical_supg_test_function(
            self.test, ubar, dt, self.advection_options["supg"])
        beqn = (
            (gammaSU*(self.trial - self.oldf + dt*self.p.Nsq*ubar[1]) 
             + dt*gammaSU*dot(ubar,grad(bbar)))*dx 
            - dt*(gammaSU('+')*dot(ubar('+'),n('+'))*bbar('+')
                  + gammaSU('-')*dot(ubar('-'),n('-'))*bbar('-'))*dS_v 
            + dt*dot(jump(gammaSU),(un('+')*bbar('+') 
                                    - un('-')*bbar('-')))*dS_v
            )
        blhs = lhs(beqn)
        brhs = rhs(beqn)

        bstarproblem = LinearVariationalProblem(blhs, brhs, self.fstar)
        self.fstarsolver = (
            LinearVariationalSolver(bstarproblem, 
                                    solver_parameters={'ksp_type':'gmres',
                                                       'pc_type':'bjacobi',
                                                       'sub_pc_type':'ilu'}))
        self.setup_diffusion()

    def setup_solver_ssprk(self):
        dt = self.p.dt
        n = self.n
        oldu, oldv, oldb, oldp = self.old.vars.split()
        newu, newv, newb, newp = self.new.vars.split()
        ubar = make_fbar(self.p.alpha, oldu, newu)
        un = 0.5*(dot(ubar,n) + abs(dot(ubar,n)))
        b = self.trial
        gammaSU = make_vertical_supg_test_function(
            self.test, ubar, dt, self.advection_options["supg"])
        blhs = gammaSU*b*dx
        brhs = (
            (gammaSU*(b - dt*self.p.Nsq*ubar[1]) 
             - dt*gammaSU*dot(ubar,grad(b)))*dx 
            + dt*(gammaSU('+')*dot(ubar('+'),n('+'))*b('+')
                  + gammaSU('-')*dot(ubar('-'),n('-'))*b('-'))*dS_v 
            - dt*dot(jump(gammaSU),(un('+')*b('+') 
                                    - un('-')*b('-')))*dS_v
            )

        bstarproblem = LinearVariationalProblem(blhs,action(brhs,self.f1),
                                                self.fstar)
        self.fstarsolver = LinearVariationalSolver(bstarproblem, 
                                                   solver_parameters
                                                   ={'ksp_type': 'gmres'})
        self.setup_diffusion()

    def setup_diffusion_interior_penulty(self):
        n = self.n
        b = self.trial
        gamma = self.test
        # parameter for interior penulty method
        mu = 1./self.p.hz
        bdifflhs = (
            gamma*b*dx + 
            self.p.dt*self.kappa*(inner(grad(gamma), grad(b))*dx 
                                    - (inner(jump(b,n), avg(grad(gamma))) + 
                                       inner(avg(grad(b)), jump(gamma,n)))*dS_v
                                    + mu*inner(jump(b,n), jump(gamma,n))*dS_v
                                    )
            )
        bdiffrhs = gamma*b*dx
        bdiffproblem = LinearVariationalProblem(bdifflhs, 
                                                action(bdiffrhs, self.fstar), 
                                                self.fstar)
        self.diffsolver = LinearVariationalSolver(bdiffproblem,
                                                  solver_parameters=
                                                  {'ksp_type': 'gmres'})

    def diffusion_interior_penulty(self):
        self.diffsolver.solve()


class PAdvection(NonlinearFieldAdvection):

    def __init__(self, p, oldf, newf, old, new, advection_options):

        NonlinearFieldAdvection.__init__(self, p, oldf, newf, old, new,
                                         advection_options)

        if p.incompressible:
            print "using incompressible formulation for p"
            self.setup_solver = getattr(self, "setup_solver_incompressible")
            self.get_residual = getattr(self, "get_residual_incompressible")

    def setup_solver_implicit(self):
        dt = self.p.dt
        phi = self.test
        psi = self.trial
        oldu, oldv, oldb, oldp = self.old.vars.split()
        newu, newv, newb, newp = self.new.vars.split()
        ubar = make_fbar(self.p.alpha, oldu, newu)
        un = 0.5*(dot(ubar,self.n) + abs(dot(ubar,self.n)))
        pbar = (1-self.p.alpha)*self.oldf + self.p.alpha*psi

        peqn = (
            (phi*(psi - self.oldf + dt*self.p.csq*div(ubar)) 
             - dt*div(phi*ubar)*pbar)*dx 
            + dt*dot(jump(phi),(un('+')*pbar('+') - un('-')*pbar('-')))*dS_v 
            + dt*dot(jump(phi),(un('+')*pbar('+') - un('-')*pbar('-')))*dS_h
            )
        plhs = lhs(peqn)
        prhs = rhs(peqn)

        pstarproblem = LinearVariationalProblem(plhs, prhs, self.fstar)
        self.fstarsolver = (
            LinearVariationalSolver(pstarproblem, 
                                    solver_parameters={'ksp_type':'gmres',
                                                       'pc_type':'bjacobi',
                                                       'sub_pc_type':'ilu'}))

    def setup_solver_ssprk(self):
        dt = self.p.dt
        n = self.n
        oldu, oldv, oldb, oldp = self.old.vars.split()
        newu, newv, newb, newp = self.new.vars.split()
        ubar = make_fbar(self.p.alpha, oldu, newu)
        un = 0.5*(dot(ubar,n) + abs(dot(ubar,n)))
        phi = self.test
        psi = self.trial

        plhs = phi*psi*dx
        prhs = (
            (phi*(psi - dt*self.p.csq*div(ubar)) + dt*div(phi*ubar)*psi)*dx 
            - dt*dot(jump(phi),(un('+')*psi('+') - un('-')*psi('-')))*dS_v 
            - dt*dot(jump(phi),(un('+')*psi('+') - un('-')*psi('-')))*dS_h
            )

        pstarproblem = LinearVariationalProblem(plhs, 
                                                action(prhs,self.f1), 
                                                self.fstar)
        self.fstarsolver = LinearVariationalSolver(pstarproblem, 
                                                   solver_parameters=
                                                   {'ksp_type': 'cg'})

    def setup_solver_incompressible(self):
        oldu, oldv, oldb, oldp = self.old.vars.split()
        newu, newv, newb, newp = self.new.vars.split()
        ubar = (1 - self.p.alpha)*oldu + self.p.alpha*newu
        pbar = (1 - self.p.alpha)*oldp + self.p.alpha*newp

        pmass = self.test*self.trial*dx
        Lpres = self.test*(div(ubar))*dx

        presproblem = LinearVariationalProblem(pmass,Lpres,self.residual)
        self.pressolver = LinearVariationalSolver(presproblem, 
                                              solver_parameters=
                                              {'ksp_type': 'cg'})

    def get_residual_incompressible(self):
        self.pressolver.solve()

class UAdvection(NonlinearFieldAdvection):

    def __init__(self, p, oldu, newu, old, new, advection_options, uinit,
                 oldeta, neweta, no_normal_flow_bc_ids):

        if advection_options["formulation"] is not "vec_invariant":
            NonlinearFieldAdvection.__init__(self, p, oldu, newu, 
                                             old, new,
                                             advection_options)
        else:
            NonlinearFieldAdvection.__init__(self, p, oldu, newu, 
                                             old, new,
                                             advection_options=None)

        self.uinit = uinit
        self.no_normal_flow_bc_ids = no_normal_flow_bc_ids

        if advection_options["formulation"] is "vec_invariant":
            print "using vector invariant formulation for u"
            self.setup_solver = getattr(self, "setup_solver_vec_invariant")
            self.get_residual = getattr(self,
                "vec_invariant_"+advection_options["scheme"])
            self.EtaAdv = EtaAdvection(p, oldeta, neweta, old, new,
                                       advection_options, uinit,
                                       no_normal_flow_bc_ids)
            self.Qperp = Function(oldu.function_space())
        elif advection_options["formulation"] is "vec_invariant_novort":
            print "using novort vector invariant formulation for u"
            self.setup_solver = getattr(self, "setup_solver_vec_invariant_novort")

    def setup_solver_vec_invariant(self):
        dt = self.p.dt
        oldu, oldv, oldb, oldp = self.old.vars.split()
        newu, newv, newb, newp = self.new.vars.split()
        ubar = make_fbar(self.p.alpha, oldu, newu)
        vbar = make_fbar(self.p.alpha, oldv, newv)
        bbar = make_fbar(self.p.alpha, oldb, newb)
        pbar = make_fbar(self.p.alpha, oldp, newp)
        w = self.test
        # setup vorticity solvers vorticity flux projection
        self.EtaAdv.setup_eta_solver()
        self.EtaAdv.setup_solver()
        self.qperp_projector = make_projection(
            perp(self.EtaAdv.Q_expr),self.Qperp)
        # set velocity residual
        umass = inner(w,self.trial)*dx
        Lures = (
            inner(w,self.newf - self.oldf) + inner(w,self.Qperp)
            - dt*inner(div(w),(pbar + 0.5*inner(ubar,ubar)))
            - dt*w[1]*(bbar - self.p.mu*ubar[1]) - dt*w[0]*self.p.f*vbar
            )*dx
        bc1 = [DirichletBC(w.function_space(), Expression(("0." , "0.")), x)
               for x in self.no_normal_flow_bc_ids]
        uresproblem = LinearVariationalProblem(umass, Lures, 
                                               self.residual,bcs=bc1)
        self.solver = LinearVariationalSolver(uresproblem, 
                                              solver_parameters=
                                              {'ksp_type': 'cg'})

    def setup_solver_vec_invariant_novort(self):
        dt = self.p.dt
        oldu, oldv, oldb, oldp = self.old.vars.split()
        newu, newv, newb, newp = self.new.vars.split()
        ubar = make_fbar(self.p.alpha, oldu, newu)
        vbar = make_fbar(self.p.alpha, oldv, newv)
        bbar = make_fbar(self.p.alpha, oldb, newb)
        pbar = make_fbar(self.p.alpha, oldp, newp)
        w = self.test
        u = self.trial
        Upwind = 0.5*(sign(dot(ubar, self.n))+1)
        # set velocity residual
        umass = inner(w,u)*dx
        Lures = (
            (inner(w,u)
             + dt*inner(perp(grad(inner(w,perp(ubar)))),u) 
             + dt*inner(div(w),(pbar + 0.5*inner(ubar,ubar)))
             + dt*w[1]*bbar
             + dt*w[0]*self.p.f*vbar
             )*dx
            - dt*2*inner(avg(inner(w,perp(ubar))*perp(self.n)),2*avg(Upwind*u))*(dS_h+dS_v)
            )

        bc1 = [DirichletBC(w.function_space(), Expression(("0." , "0.")), x)
               for x in self.no_normal_flow_bc_ids]
        uresproblem = LinearVariationalProblem(umass, action(Lures,self.f1), 
                                               self.fstar,bcs=bc1)
        self.fstarsolver = LinearVariationalSolver(uresproblem, 
                                              solver_parameters=
                                              {'ksp_type': 'cg'})
        self.setup_diffusion()

    def setup_diffusion_interior_penulty(self):
        print "in setup_diffusion_interior_penulty for u"
        n = self.n
        u = self.trial
        w = self.test
        # parameter for interior penulty method
        mu = 1./self.p.hx
        # viscosity coefficient
        muM = as_tensor([[self.kappa,0],[0,self.kappa]])

        both = lambda u: 2*avg(u)
    
        a_viscous = inner(grad(u),dot(grad(w),muM))*dx

        def get_flux_form(dS,mu):
            a_viscous_fluxes = (-inner(both(outer(w,n)),avg(dot(grad(u),muM)))
                                 -inner(both(outer(u,n)),avg(dot(grad(w),muM)))
                                 +mu*inner(
                    both(outer(w,n)),dot(both(outer(u,n)),muM)))*dS

            return a_viscous_fluxes
        
        udifflhs = inner(w,u)*dx + self.p.dt*(
            a_viscous + get_flux_form(dS_v,mu) + get_flux_form(dS_h,mu))

        udiffrhs = inner(w,u)*dx
        bc1 = [DirichletBC(w.function_space(), Expression(("0." , "0.")), x)
               for x in self.no_normal_flow_bc_ids]
        udiffproblem = LinearVariationalProblem(udifflhs, 
                                                action(udiffrhs, self.fstar), 
                                                self.fstar, bcs=bc1)
        self.diffsolver = LinearVariationalSolver(udiffproblem,
                                            solver_parameters={
                                                'ksp_type':'cg',
                                                'pc_type':'sor'})

    def diffusion_interior_penulty(self):
        self.diffsolver.solve()


    def setup_solver_ssprk(self):
        dt = self.p.dt
        oldu, oldv, oldb, oldp = self.old.vars.split()
        newu, newv, newb, newp = self.new.vars.split()
        ubar = make_fbar(self.p.alpha, oldu, newu)
        vbar = make_fbar(self.p.alpha, oldv, newv)
        bbar = make_fbar(self.p.alpha, oldb, newb)
        pbar = make_fbar(self.p.alpha, oldp, newp)
        un = 0.5*(dot(ubar,self.n) + abs(dot(ubar,self.n)))
        w = self.test
        u = self.trial

        ulhs = inner(w,u)*dx
        urhs = (
            (inner(w,u) + dt*(ubar[j]*w[i]).dx(j)*u[i]
            + dt*inner(div(w),pbar)
            + dt*w[1]*(bbar - self.p.mu*ubar[1]) + dt*w[0]*self.p.f*vbar
            )*dx
            - dt*( dot(jump(w), un('+')*u('+') - un('-')*u('-')) )*(dS_h+dS_v)
            )
        bc1 = [DirichletBC(u.function_space(), Expression(("0." , "0.")), x)
               for x in self.no_normal_flow_bc_ids]
        ustarproblem = LinearVariationalProblem(ulhs, 
                                               action(urhs, self.f1), 
                                               self.fstar,bcs=bc1)
        self.fstarsolver = LinearVariationalSolver(ustarproblem, 
                                                   solver_parameters=
                                                   {'ksp_type': 'cg'})

    def vec_invariant_ssprk(self):
        self.EtaAdv.etasolver.solve()
        self.EtaAdv.ssprk()
        self.qperp_projector.solve()
        self.solver.solve()
            
    def vec_invariant_implicit(self):
        self.EtaAdv.etasolver.solve()
        self.EtaAdv.implicit()
        self.qperp_projector.solve()
        self.solver.solve()

class VAdvection(NonlinearFieldAdvection):

    def __init__(self, p, oldf, newf, old, new, advection_options, uinit,
                 oldomega, newomega, no_normal_flow_bc_ids):

        NonlinearFieldAdvection.__init__(self, p, oldf, newf, old, new,
                                         advection_options)
        self.uinit = uinit
        self.no_normal_flow_bc_ids = no_normal_flow_bc_ids

        if advection_options["formulation"] is "vec_invariant":
            print "using vector invariant formulation for v"
            self.setup_solver = getattr(self, "setup_solver_vec_invariant")
            self.get_residual = getattr(self,
                "vec_invariant_"+advection_options["scheme"])
            self.OmegaAdv = OmegaAdvection(p, oldomega, newomega, old, new,
                                       advection_options, 
                                       no_normal_flow_bc_ids)
            self.Qv = Function(oldf.function_space())

    def setup_solver_vec_invariant(self):
        self.OmegaAdv.setup_omega_solver()
        self.OmegaAdv.setup_solver()
        self.qv_projector = make_projection(
            self.OmegaAdv.Qv_expr, self.Qv)
        vlhs = self.test*self.trial*dx
        vrhs = self.test*(self.oldf - self.Qv)*dx
        vresproblem = LinearVariationalProblem(vlhs, vrhs, self.residual)
        self.solver = LinearVariationalSolver(vresproblem, 
                                              solver_parameters=
                                              {'ksp_type': 'cg'})

    def setup_solver_implicit(self):
        dt = self.p.dt
        oldu, oldv, oldb, oldp = self.old.vars.split()
        newu, newv, newb, newp = self.new.vars.split()
        ubar = make_fbar(self.p.alpha, oldu, newu)
        un = 0.5*(dot(ubar,self.n) + abs(dot(ubar,self.n)))
        wv = self.test
        v = self.trial
        vbar = (1-self.p.alpha)*self.oldf + self.p.alpha*v

        veqn = (
            (wv*(v - self.oldf + dt*self.p.f*(ubar[0]-self.uinit[0])) 
             - dt*div(wv*ubar)*vbar)*dx 
            + dt*dot(jump(wv),(un('+')*vbar('+') - un('-')*vbar('-')))*dS_v 
            + dt*dot(jump(wv),(un('+')*vbar('+') - un('-')*vbar('-')))*dS_h
            )
        vlhs = lhs(veqn)
        vrhs = rhs(veqn)
        vstarproblem = LinearVariationalProblem(vlhs, vrhs, self.fstar)
        self.fstarsolver = LinearVariationalSolver(vstarproblem, 
                                                   solver_parameters=
                                                   {'ksp_type': 'cg'})

    def setup_solver_ssprk(self):
        dt = self.p.dt
        oldu, oldv, oldb, oldp = self.old.vars.split()
        newu, newv, newb, newp = self.new.vars.split()
        ubar = make_fbar(self.p.alpha, oldu, newu)
        un = 0.5*(dot(ubar,self.n) + abs(dot(ubar,self.n)))
        wv = self.test
        v = self.trial

        vlhs = wv*v*dx
        vrhs = (
            (wv*(v - dt*self.p.f*(ubar[0]-self.uinit[0])) 
             + dt*div(wv*ubar)*v)*dx 
            - dt*dot(jump(wv),(un('+')*v('+') - un('-')*v('-')))*dS_v 
            - dt*dot(jump(wv),(un('+')*v('+') - un('-')*v('-')))*dS_h
            )
        vstarproblem = LinearVariationalProblem(vlhs, 
                                                action(vrhs,self.f1), 
                                                self.fstar)
        self.fstarsolver = LinearVariationalSolver(vstarproblem, 
                                                   solver_parameters=
                                                   {'ksp_type': 'cg'})

    def vec_invariant_implicit(self):
        self.OmegaAdv.omegasolver.solve()
        self.OmegaAdv.implicit()
        self.qv_projector.solve()
        self.solver.solve()

class EtaAdvection(NonlinearFieldAdvection):

    def __init__(self, p, oldeta, neweta, old, new, advection_options, uinit, 
                 no_normal_flow_bc_ids):

        print "using", advection_options["scheme"], "for eta"
        NonlinearFieldAdvection.__init__(self, p, oldeta, neweta, old, new, 
                                         advection_options)

        self.uinit = uinit
        self.no_normal_flow_bc_ids = no_normal_flow_bc_ids

    def setup_eta_solver(self):
        oldu, oldv, oldb, oldp = self.old.vars.split()
        aeta = self.test*self.trial*dx
        Leta = -dot(curl(self.test),oldu)*dx
        bc1 = [DirichletBC(self.test.function_space(), Expression("0."), x)
               for x in self.no_normal_flow_bc_ids]
        etaproblem = LinearVariationalProblem(aeta, Leta, self.oldf, bcs=bc1)
        self.etasolver = LinearVariationalSolver(etaproblem, 
                                             solver_parameters=
                                                 {'ksp_type': 'cg'})

    def setup_solver_implicit(self):
        p = self.p
        oldu, oldv, oldb, oldp = self.old.vars.split()
        newu, newv, newb, newp = self.new.vars.split()
        ubar = make_fbar(p.alpha, oldu, newu)
        vbar = make_fbar(p.alpha, oldv, newv)
        bbar = make_fbar(p.alpha, oldb, newb)
        etastar = (1-p.alpha)*self.oldf + p.alpha*self.trial
        sigmaSU, dsigma, tau = make_supg_test_function(
            self.test, ubar, p.dt, self.advection_options["supg"])
        Eadv = (
            sigmaSU*(self.trial-self.oldf)
            - p.dt*inner(
                grad(self.test),
                (ubar*etastar 
                 + as_vector((bbar - p.mu*ubar[1],-p.f*vbar))
                ))
            + p.dt*dsigma*(div(ubar*etastar
                               + as_vector((bbar - p.mu*ubar[1], -p.f*vbar))
                               ))
            )*dx
        aadv = lhs(Eadv)
        Ladv = rhs(Eadv)
        bc1 = [DirichletBC(self.test.function_space(), Expression("0."), x)
               for x in self.no_normal_flow_bc_ids]
        # Set up vorticity advection solver
        etaadvproblem = LinearVariationalProblem(aadv, Ladv, self.newf, bcs=bc1)
        self.fstarsolver = LinearVariationalSolver(etaadvproblem, 
                                             solver_parameters=
                                                    {'ksp_type': 'cg'})
        # save Q_expr
        etabar = (1-p.alpha)*self.oldf + p.alpha*self.newf
        self.Q_expr = (
            -p.dt*ubar*etabar
             + dot(ubar,tau)*(
                (self.newf - self.oldf) + 
                p.dt*(div(ubar*self.newf
                          + as_vector((bbar - p.mu*ubar[1], -p.f*vbar)))))
             )

    def setup_solver_ssprk(self):
        p = self.p
        function_space = self.test.function_space()
        self.eta_st1 = Function(function_space)
        self.eta_st2 = Function(function_space)
        oldu, oldv, oldb, oldp = self.old.vars.split()
        newu, newv, newb, newp = self.new.vars.split()
        ubar = make_fbar(p.alpha, oldu, newu)
        vbar = make_fbar(p.alpha, oldv, newv)
        bbar = make_fbar(p.alpha, oldb, newb)
        eta = self.trial
        #
        sigmaSU, dsigma, tau = make_supg_test_function(
            self.test, ubar, p.dt, self.advection_options["supg"])
        aadv = sigmaSU*eta*dx
        Ladv = (
            (sigmaSU*eta 
             + p.dt*inner(grad(self.test),
                          (ubar*eta 
                           + as_vector((bbar - p.mu*ubar[1], -p.f*vbar))))
             - p.dt*inner(dsigma,div(ubar*eta + 
                                     as_vector((bbar - p.mu*ubar[1],
                                                -p.f*vbar))))
             )*dx
            )
        bc1 = [DirichletBC(function_space, Expression("0."), x)
               for x in self.no_normal_flow_bc_ids]
        # Set up vorticity advection solver
        etaadvproblem = LinearVariationalProblem(aadv, 
                                                 action(Ladv,self.f1), 
                                                 self.fstar, bcs=bc1)
        self.fstarsolver = LinearVariationalSolver(etaadvproblem, 
                                             solver_parameters=
                                                    {'ksp_type': 'cg'})
        self.Q_expr = (
            -(1.0/6.0)*p.dt*ubar*(self.oldf + self.eta_st1 + 4*self.eta_st2)
            + dot(ubar,tau)*(
                (self.newf - self.oldf) + 
                (1.0/6.0)*p.dt*(div(
                        ubar*(self.oldf + self.eta_st1 + 4*self.eta_st2)
                        + as_vector((bbar - p.mu*ubar[1], -p.f*vbar)))))
             )

    def setup_diffusion_cg(self):
        eta = self.trial
        sigma = self.test
        difflhs = (sigma*eta 
                   + self.p.dt*self.kappa*dot(grad(sigma),grad(eta)))*dx
        diffrhs = sigma*eta*dx
        bc1 = [DirichletBC(eta.function_space(), Expression("0."), x)
               for x in self.no_normal_flow_bc_ids]
        etadiffproblem = LinearVariationalProblem(
            difflhs, action(diffrhs,self.newf), self.newf, bcs=bc1)
        self.diffsolver = LinearVariationalSolver(
            etadiffproblem, solver_parameters={'ksp_type': 'cg'})
        self.Q_expr += self.p.dt*self.kappa*perp(grad(self.newf))

    def diffusion_cg(self):
        self.diffsolver.solve()

    def ssprk(self):
        f1 = self.f1
        f1.assign(self.oldf)
        self.fstarsolver.solve()
        f1.assign(self.fstar)
        self.eta_st1.assign(f1)
        self.fstarsolver.solve()
        f1.assign(0.75*self.oldf + 0.25*self.fstar)
        self.eta_st2.assign(f1)
        self.fstarsolver.solve()
        self.newf.assign((1.0/3.0)*self.oldf + (2.0/3.0)*self.fstar)

class OmegaAdvection(NonlinearFieldAdvection):

    def __init__(self, p, oldomega, newomega, old, new, advection_options, 
                 no_normal_flow_bc_ids):

        print "using", advection_options, "for omega"
        NonlinearFieldAdvection.__init__(self, p, oldomega, newomega, old, new, 
                                         advection_options)

        self.no_normal_flow_bc_ids = no_normal_flow_bc_ids

    def setup_omega_solver(self):
        oldu, oldv, oldb, oldp = self.old.vars.split()
        aomega = inner(self.test,self.trial)*dx
        Lomega = (inner(curl(self.test),oldv) 
                  + inner(self.test,as_vector((0.,self.p.f))))*dx
        bc1 = [DirichletBC(self.test.function_space(),
                           Expression(("0.","0.")), x)
               for x in self.no_normal_flow_bc_ids]
        omegaproblem = LinearVariationalProblem(aomega, Lomega, 
                                                self.oldf, bcs=bc1)
        self.omegasolver = LinearVariationalSolver(omegaproblem, 
                                                   solver_parameters=
                                                   {'ksp_type': 'cg'})

    def setup_solver_implicit(self):
        oldu, oldv, oldb, oldp = self.old.vars.split()
        newu, newv, newb, newp = self.new.vars.split()
        ubar = make_fbar(self.p.alpha, oldu, newu)
        Sigma = self.test
        omega = self.trial
        omegastar = (1-self.p.alpha)*self.oldf + self.p.alpha*omega
        Eadv = (inner(Sigma,(omega-self.oldf))
                 - self.p.dt*inner(curl(Sigma),
                              dot((omegastar 
                                   + as_vector((0.,self.p.f))),perp(ubar))))*dx
        aadv = lhs(Eadv)
        Ladv = rhs(Eadv)
        bc1 = [DirichletBC(Sigma.function_space(), Expression(("0.", "0.")), x)
               for x in self.no_normal_flow_bc_ids]
        # Set up vorticity advection solver
        problem = LinearVariationalProblem(aadv, Ladv, self.newf, bcs=bc1)
        self.fstarsolver = LinearVariationalSolver(problem, 
                                              solver_parameters=
                                              {'ksp_type': 'cg'})

        # Calculate Qv
        omegabar = (1-self.p.alpha)*self.oldf + self.p.alpha*self.newf
        self.Qv_expr = -self.p.dt*dot(
            omegabar + as_vector((0.,self.p.f)),perp(ubar))


class NonlinearSlice(LinearSlice):

    def __init__(self, setup):

        LinearSlice.__init__(self, setup)

        self.parameters = setup
        p = self.parameters

        #In-slice vorticity (eta) space
        V0_elt = TensorProductElement(self.IUh0,self.IUv0)
        self.V0 = FunctionSpace(self.mesh,V0_elt)

        #Out-of-slice vorticity (omega) space
        V1perp_elt = HCurl(self.V1_v) + HCurl(self.V1_h)
        V1perp = FunctionSpace(self.mesh,V1perp_elt)

        self.old.eta = Function(self.V0)
        self.new.eta = Function(self.V0)

        # Out-of-slice vorticity (omega) space
        self.old.omega = Function(V1perp)
        self.new.omega = Function(V1perp)

        # add vorticity and vorticity flux to fields dictionary incase
        # we want to dump them
        self.field_dict["eta"] = self.old.eta
        self.field_dict["omega"] = self.old.omega
#        self.field_dict["Qv"] = self.Qv

        # For SUPG and IP
        p.hx = p.L/p.ncolumns
        p.hz = p.H/p.nlayers

        oldu, oldv, oldb, oldp = self.old.vars.split()
        newu, newv, newb, newp = self.new.vars.split()

        self.field_advection = {
            "uw": UAdvection(p, oldu, newu, self.old, self.new,
                            p.advection_options["uw"], self.uinit,
                            self.old.eta, self.new.eta,
                            self.no_normal_flow_bc_ids),
            "v": VAdvection(p, oldv, newv, self.old, self.new,
                            p.advection_options["v"], self.uinit,
                            self.old.omega, self.new.omega,
                            self.no_normal_flow_bc_ids),
            "b": BAdvection(p, oldb, newb, self.old, self.new,
                            p.advection_options["b"]),
            "p": PAdvection(p, oldp, newp, self.old, self.new,
                            p.advection_options["p"]),
            }

        self.ures = self.field_advection["uw"].residual
        self.vres = self.field_advection["v"].residual
        self.bres = self.field_advection["b"].residual
        self.pres = self.field_advection["p"].residual

    def initialise(self):

        p = self.parameters
        if p.initial_conditions["p"] is "hydrostatic":
            self.hydrostatic_initial_pressure()

        if p.initial_conditions["v"] is "geostrophic":
            self.geostrophic_initial_velocity()

