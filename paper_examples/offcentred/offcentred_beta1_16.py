from nonlinear_eady_slice import NonlinearEadySlice, NonlinearEadySliceConfiguration
from firedrake import *
import inifns
import pickle

L = 1000000.
H = 10000.
f = 1.0e-4
Nsq = 2.5e-05
Bu = 0.5

nlayers = 30
ncolumns = 60
dt = 25.
end = 30*24*60*60.
Tdump = 24*60*60. 
Ddump = 2*60*60.   

a = -7.5
alpha = 0.55
beta = 1./16.

f = f/beta
L = beta*L
dstep = int(Ddump/dt)

# set up initial conditions
template_s = inifns.template_target_strings()
b0_exp = Expression(template_s,a=a,Nsq=Nsq,Bu=Bu,H=H,L=L)

setup = NonlinearEadySliceConfiguration(
    L=2*L,H=H,nlayers=nlayers,ncolumns=ncolumns,
    dt=dt,end=end,Tdump=Tdump,dstep=dstep, 
    Nsq=Nsq,alpha=alpha,beta=beta,f=f,
    initial_conditions = {"b": b0_exp, "p": "hydrostatic", "v":"geostrophic"},
    output_fields=['b','v','vtres'],
    advection_options = {
        "uw":{"formulation":"vec_invariant_novort","scheme":"ssprk"}
    },
    incompressible=True,
    diagnostics = {"geostrophic_imbalance":[], "eady_potential_energy":[],
                   "eady_total_energy":[], "true_residual_v":[]},
    diagnostics_to_print = ["kinetic_energy_u", "kinetic_energy_v", "eady_potential_energy",
                            "eady_total_energy", "geostrophic_imbalance", "courant_number_x",
                            "courant_number_z","true_residual_v"],
    ksp_atol = 1.e-08,
    ksp_rtol = 1.e-08,
    ksp_monitor_true_residual=False,
    dirstr='paper_results/offcentred/',
    dirname='beta1_16')

Model = NonlinearEadySlice(setup)
data, dirs = Model.run()

pickle.dump(data, open("%s/%s.p" % (dirs, "data"), "wb"))
