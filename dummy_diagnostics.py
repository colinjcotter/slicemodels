from diagnostics import *

def kinetic_energy_v_dummy(p, old_d):
    oldu_d, oldv_d, oldb_d, oldp_d = old_d.vars.split()
    dummy = assemble(0.5*(oldv_d*oldv_d)*dx)
    return dummy

def kinetic_energy_v_dummy_sum(dummy_list):
    if len(dummy_list) == 0:
        dummy_sum = 0.
    else:
        dummy_sum = dummy_list[-1]
    return dummy_sum
