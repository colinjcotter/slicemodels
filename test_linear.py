from slicemodels import SliceConfiguration, LinearSlice
from firedrake import *

b0_exp = Expression("0.01*sin(pi*x[1]/10000.)/(1+pow(x[0]-150000.,2)/pow(5000.,2))")

dt = 6.

setup = SliceConfiguration(
    L=300000.,H=10000.,nlayers=10,ncolumns=300,
    dt=dt, end=3000.,Tdump=10*dt,
    output_fields = ['b'],
    initial_conditions = {'b': b0_exp}, 
    Nsq=1.e-4, f=1.e-4, nits=2,
    incompressible=True,
    filename='test_linear_incompressible')

Model = LinearSlice(setup)

Model.run()
