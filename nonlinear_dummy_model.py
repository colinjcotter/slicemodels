from nonlinear_slice import VAdvection
from utility_functions import *
from firedrake import *
import sys
import numpy as np

class DummyVAdvection(VAdvection):

    def __init__(self, p, oldf, newf, old, new, advection_options, uinit,
                 oldomega, newomega, no_normal_flow_bc_ids):

        VAdvection.__init__(self, p, oldf, newf, old, new, advection_options, uinit,
                            oldomega, newomega, no_normal_flow_bc_ids)

    def setup_solver_implicit(self):
        print "in setup_solver_implicit in DummyVAdvection"
        dt = self.p.dt
        oldu, oldv, oldb, oldp = self.old.vars.split()
        newu, newv, newb, newp = self.new.vars.split()
        ubar = make_fbar(self.p.alpha, oldu, newu)
        un = 0.5*(dot(ubar,self.n) + abs(dot(ubar,self.n)))
        # Expression for Forcing
        V2 = oldp.function_space()
        Forcing = Function(V2).interpolate(Expression(("x[1]-H/2"),H=self.p.H))
        wv = self.test
        v = self.trial
        vbar = (1-self.p.alpha)*self.oldf + self.p.alpha*v

        veqn = (
            (wv*(v - self.oldf) 
             - dt*div(wv*ubar)*vbar)*dx 
            + dt*dot(jump(wv),(un('+')*vbar('+') - un('-')*vbar('-')))*dS_v 
            + dt*dot(jump(wv),(un('+')*vbar('+') - un('-')*vbar('-')))*dS_h
            )
        vlhs = lhs(veqn)
        vrhs = rhs(veqn)
        vstarproblem = LinearVariationalProblem(vlhs, vrhs, self.fstar)
        self.fstarsolver = LinearVariationalSolver(vstarproblem, 
                                                   solver_parameters=
                                                   {'ksp_type': 'cg'})

    def setup_solver_ssprk(self):
        print "in setup_solver_ssprk in DummyVAdvection"
        dt = self.p.dt
        oldu, oldv, oldb, oldp = self.old.vars.split()
        newu, newv, newb, newp = self.new.vars.split()
        ubar = make_fbar(self.p.alpha, oldu, newu)
        un = 0.5*(dot(ubar,self.n) + abs(dot(ubar,self.n)))
        # Expression for Forcing
        V2 = oldp.function_space()
        Forcing = Function(V2).interpolate(Expression(("x[1]-H/2"),H=self.p.H))
        wv = self.test
        v = self.trial

        vlhs = wv*v*dx
        vrhs = (
            (wv*v
             + dt*div(wv*ubar)*v)*dx 
            - dt*dot(jump(wv),(un('+')*v('+') - un('-')*v('-')))*dS_v 
            - dt*dot(jump(wv),(un('+')*v('+') - un('-')*v('-')))*dS_h
            )
        vstarproblem = LinearVariationalProblem(vlhs, 
                                                action(vrhs,self.f1), 
                                                self.fstar)
        self.fstarsolver = LinearVariationalSolver(vstarproblem, 
                                                   solver_parameters=
                                                   {'ksp_type': 'cg'})

