"""
Some simple tools for making model configuration nicer.
"""


class Configuration(object):
    def __init__(self, **kwargs):

        self.__setattr__ = self._proto__setattr__
        for name, value in kwargs.iteritems():
            self.__setattr__(name, value)

    def _proto__setattr__(self, name, value):
        """Cause setting an unknown attribute to be an error"""
        self.__getattribute__(name)
        object.__setattr__(self, name, value)

### Example configuration starts here.

if __name__=="__main__":

    class MyConfiguration(Configuration):

        #: Documentation for myparam
        myparam = None
    
        #: As in GEM, manual suggests 0.1
        dt = 0.1

    c = MyConfiguration(dt=2)

    print c.dt
