from nonlinear_slice import NonlinearSlice, UAdvection, PAdvection
from nonlinear_dummy_slice import NonlinearDummySlice
from nonlinear_eady_model import EadyBAdvection, EadyVAdvection
from nonlinear_dummy_model import DummyVAdvection
from eady_tools import *
from utility_functions import *
from firedrake import *
import sys
import numpy as np
op2.init(log_level='WARNING')
from firedrake.petsc import PETSc

class NonlinearEadyDummySlice(NonlinearDummySlice):

    def __init__(self, setup):

        NonlinearDummySlice.__init__(self, setup)

        self.diagnostics_mod = "eady_diagnostics"

        p = self.parameters

        # set field_advection
        oldu, oldv, oldb, oldp = self.old.vars.split()
        newu, newv, newb, newp = self.new.vars.split()

        self.field_advection = {
            "uw": UAdvection(p, oldu, newu, self.old, self.new,
                            p.advection_options["uw"], self.uinit,
                            self.old.eta, self.new.eta,
                            self.no_normal_flow_bc_ids),
            "v": EadyVAdvection(p, oldv, newv, self.old, self.new,
                            p.advection_options["v"], self.uinit,
                            self.old.omega, self.new.omega,
                            self.no_normal_flow_bc_ids),
            "b": EadyBAdvection(p, oldb, newb, self.old, self.new,
                            p.advection_options["b"]),
            "p": PAdvection(p, oldp, newp, self.old, self.new,
                            p.advection_options["p"]),
            }

        self.ures = self.field_advection["uw"].residual
        self.vres = self.field_advection["v"].residual
        self.bres = self.field_advection["b"].residual
        self.pres = self.field_advection["p"].residual

        # set field_advection_d
        oldu_d, oldv_d, oldb_d, oldp_d = self.old_d.vars.split()
        newu_d, newv_d, newb_d, newp_d = self.new_d.vars.split()

        self.field_advection_d = {
            "uw": UAdvection(p, oldu_d, newu_d, self.old_d, self.new_d,
                            p.advection_options["uw"], self.uinit,
                            self.old.eta, self.new.eta,
                            self.no_normal_flow_bc_ids),
            "v": DummyVAdvection(p, oldv_d, newv_d, self.old_d, self.new_d,
                            p.advection_options["v"], self.uinit,
                            self.old.omega, self.new.omega,
                            self.no_normal_flow_bc_ids),
            "b": EadyBAdvection(p, oldb_d, newb_d, self.old_d, self.new_d,
                            p.advection_options["b"]),
            "p": PAdvection(p, oldp_d, newp_d, self.old_d, self.new_d,
                            p.advection_options["p"]),
            }

        self.ures_d = self.field_advection_d["uw"].residual
        self.vres_d = self.field_advection_d["v"].residual
        self.bres_d = self.field_advection_d["b"].residual
        self.pres_d = self.field_advection_d["p"].residual

    def initialise(self):
        # get parameters
        p = self.parameters
        oldu, oldv, oldb, oldp = self.old.vars.split()
        # balanced initialization
        if p.initial_conditions["p"] is "hydrostatic":
            self.hydrostatic_initial_pressure()
            self.pressure_boundary_correction()
        if p.initial_conditions["v"] is "geostrophic":
            self.geostrophic_initial_velocity()
            solve_balanced_velocity(self.parameters,self.old)
        # setup a list for dummy energy
        self.kineticv_dummy_sum = []
        # setup variables and solvers for diagnostics
        if "geostrophic_imbalance" in p.diagnostics.keys():
            self.field_dict["geo"] = Function(oldu.function_space())
            self.diagnostic_fields_solver_dict["geo"] = setup_geostrophic_imbalance_solver(self.parameters,self.old,
                                                                                           self.field_dict["geo"])
            p.diagnostics["geostrophic_imbalance"] = ["geostrophic_imbalance",
                                                      {"p": p, "geo": self.field_dict["geo"]}]
        if "eady_potential_energy" in p.diagnostics.keys():
            p.diagnostics["eady_potential_energy"] = ["eady_potential_energy", {"p": p, "old": self.old}] 
        if "eady_total_energy" in p.diagnostics.keys():
            p.diagnostics["eady_total_energy"] = ["eady_total_energy", {"p": p, "old": self.old}] 
        if "true_residual_v" in p.diagnostics.keys():
            V = FunctionSpace(oldp.function_space().mesh(), "DG", 0)
            self.vtres = Function(V)
            self.field_dict["vtres"] = self.vtres
            self.diagnostic_fields_solver_dict["vtres"] = setup_vtres_solver(self.parameters,self.old,self.new,
                                                                             self.field_dict["vtres"])
            p.diagnostics["true_residual_v"] = ["true_residual_v",
                                                {"p": p, "vtres": self.field_dict["vtres"]}]
        if "kinetic_energy_v_dummy" in p.diagnostics.keys():
            p.diagnostics["kinetic_energy_v_dummy"] = ["kinetic_energy_v_dummy", {"p": p, "old_d": self.old_d}]
        if "kinetic_energy_v_dummy_sum" in p.diagnostics.keys():
            p.diagnostics["kinetic_energy_v_dummy_sum"] = ["kinetic_energy_v_dummy_sum", {"dummy_list": self.kineticv_dummy_sum}]

    def print_diagnostics(self):
        # print selected diagnostics every dstep
        for field in self.diagnostics_to_print:
            self.mpiprint(field+" = {}".format(self.Data[field]))
        self.mpiprint("RMSV = {}".format(self.Data["v"]["rms"]))
        self.mpiprint("Vmax = {}".format(self.Data["v"]["max"]))

