from slicemodels import LinearSlice, SliceConfiguration, UAdvection, VAdvection, BAdvection, PAdvection
from eady_tools import *
from firedrake import *
import sys
import numpy as np

class LinearEadySliceConfiguration(SliceConfiguration):

    beta = None
    dbdy = -10./300.*3.0e-06

    def __init__(self, **kwargs):

        SliceConfiguration.__init__(self, **kwargs)


class EadyUAdvection(UAdvection):

    def __init__(self, p, oldf, newf, old, new, uinit, uadv, un,
                 no_normal_flow_bc_ids):

        UAdvection.__init__(self, p, oldf, newf, old, new, uinit,
                            no_normal_flow_bc_ids)
        self.uadv = uadv
        self.un = un

    def setup_vorticity_solver(self):
        oldu, oldv, oldb, oldp = self.old.vars.split()

        V0 = self.old.eta.function_space()
        z = TrialFunction(V0)
        sigma = TestFunction(V0)

        bc1 = [DirichletBC(V0, Expression("0."), x)
               for x in ["top", "bottom"]]

        az = sigma*z*dx
        Lz = -dot(curl(sigma),oldu)*dx
        zetaproblem = LinearVariationalProblem(az, Lz, self.old.eta, bcs=bc1)
        self.zetasolver = LinearVariationalSolver(zetaproblem, 
                                             solver_parameters={'ksp_type': 'cg'})

    def setup_vorticity_advection(self):
        oldu, oldv, oldb, oldp = self.old.vars.split()
        newu, newv, newb, newp = self.new.vars.split()
        oldz = self.old.eta

        V0 = self.old.eta.function_space()
        z = TrialFunction(V0)
        sigma = TestFunction(V0)

        bbar = (1 - self.p.alpha)*oldb + self.p.alpha*newb
        zbar = (1 - self.p.alpha)*oldz + self.p.alpha*z 

        bc1 = [DirichletBC(V0, Expression("0."), x)
               for x in ["top", "bottom"]]

        Ezadv = (sigma*(z-oldz) - self.p.dt*sigma.dx(0)*(self.uadv[0]*zbar + bbar))*dx
        azadv = lhs(Ezadv)
        Lzadv = rhs(Ezadv)

        zadvproblem = LinearVariationalProblem(azadv, Lzadv, self.new.eta, bcs=bc1)
        self.zadvsolver = LinearVariationalSolver(zadvproblem, 
                                             solver_parameters={'ksp_type': 'cg'})

    def setup_solver(self):
        oldu, oldv, oldb, oldp = self.old.vars.split()
        newu, newv, newb, newp = self.new.vars.split()
        newz = self.new.eta
        oldz = self.old.eta

        ubar = (1 - self.p.alpha)*oldu + self.p.alpha*newu
        vbar = (1 - self.p.alpha)*oldv + self.p.alpha*newv
        bbar = (1 - self.p.alpha)*oldb + self.p.alpha*newb
        pbar = (1 - self.p.alpha)*oldp + self.p.alpha*newp
        zbar = (1 - self.p.alpha)*oldz + self.p.alpha*newz

        test = self.test
        dt = self.p.dt

        self.setup_vorticity_solver()
        self.setup_vorticity_advection()
        Qperp = -newz*self.uadv[0]

        umass = inner(test,self.trial)*dx
        Lures = (inner(test,self.newf - self.oldf) + dt*test[1]*Qperp
                 - dt*self.p.f*test[0]*vbar
                 - dt*(div(test)*(pbar + self.uadv[0]*ubar[0]) + test[1]*bbar))*dx

        bc1 = [DirichletBC(test.function_space(), Expression(("0.","0.")), 
                           bdry_id) for bdry_id in self.no_normal_flow_bc_ids]
        uresproblem = LinearVariationalProblem(umass, Lures, 
                                               self.residual, bcs=bc1)
        self.solver = LinearVariationalSolver(uresproblem, 
                                              solver_parameters=
                                              {'ksp_type': 'cg'})

    def get_residual(self):
        self.zetasolver.solve()
        self.zadvsolver.solve()
        self.solver.solve()


class EadyVAdvection(VAdvection):

    def __init__(self, p, oldf, newf, old, new, uinit, uadv, un):

        VAdvection.__init__(self, p, oldf, newf, old, new, uinit)
        self.uadv = uadv
        self.un = un

    def setup_solver(self):
        oldu, oldv, oldb, oldp = self.old.vars.split()
        newu, newv, newb, newp = self.new.vars.split()
        ubar = (1 - self.p.alpha)*oldu + self.p.alpha*newu
        vbar = (1 - self.p.alpha)*oldv + self.p.alpha*newv
        # Expression for Forcing
        Forcing = Function(oldv.function_space()).interpolate(Expression(("x[1]-H/2"),H=self.p.H))
        vmass = self.test*self.trial*dx
        Lvres = (
            self.test*(self.newf-self.oldf) + 
            self.p.dt*self.p.f*self.test*(ubar[0]-self.uinit[0]) +
            self.p.dt*self.test*self.p.dbdy*Forcing -
            self.p.dt*self.uadv[0]*self.test.dx(0)*vbar
            )*dx + self.p.dt*dot(jump(self.test),(self.un('+')*vbar('+') - self.un('-')*vbar('-')))*dS_v

        vresproblem = LinearVariationalProblem(vmass, Lvres, self.residual)
        self.solver = LinearVariationalSolver(vresproblem, 
                                              solver_parameters=
                                              {'ksp_type': 'cg'})

class EadyBAdvection(BAdvection):

    def __init__(self, p, oldf, newf, old, new, uadv, un):

        BAdvection.__init__(self, p, oldf, newf, old, new)
        self.uadv = uadv
        self.un = un

    def setup_solver(self):

        oldu, oldv, oldb, oldp = self.old.vars.split()
        newu, newv, newb, newp = self.new.vars.split()
        ubar = (1 - self.p.alpha)*oldu + self.p.alpha*newu
        vbar = (1 - self.p.alpha)*oldv + self.p.alpha*newv
        bbar = (1 - self.p.alpha)*oldb + self.p.alpha*newb
        bmass = self.test*self.trial*dx

        Lbres = (
            self.test*(self.newf-self.oldf + self.p.dt*self.p.Nsq*ubar[1] + self.p.dt*self.p.dbdy*vbar)
            - self.p.dt*self.uadv[0]*self.test.dx(0)*bbar 
            )*dx + self.p.dt*dot(jump(self.test),(self.un('+')*bbar('+') - self.un('-')*bbar('-')))*dS_v 

        bresproblem = LinearVariationalProblem(bmass,Lbres,self.residual)
        self.solver = LinearVariationalSolver(bresproblem, 
                                              solver_parameters=
                                              {'ksp_type': 'cg'})

class EadyPAdvection(PAdvection):

    def __init__(self, p, oldf, newf, old, new, uadv, un):

        PAdvection.__init__(self, p, oldf, newf, old, new)
        self.uadv = uadv
        self.un = un

    def setup_solver(self):

        oldu, oldv, oldb, oldp = self.old.vars.split()
        newu, newv, newb, newp = self.new.vars.split()
        ubar = (1 - self.p.alpha)*oldu + self.p.alpha*newu
        pbar = (1 - self.p.alpha)*oldp + self.p.alpha*newp
        pmass = self.test*self.trial*dx

        if self.p.incompressible:
            Lpres = self.test*(div(ubar))*dx
        else:
            Lpres = (
                self.test*(self.newf-self.oldf + self.p.dt*self.p.csq*div(ubar))
                - self.p.dt*self.uadv[0]*(self.test.dx(0)*pbar)
                )*dx + self.p.dt*dot(jump(self.test),(self.un('+')*pbar('+') - self.un('-')*pbar('-')))*dS_v

        presproblem = LinearVariationalProblem(pmass,Lpres,self.residual)
        self.solver = LinearVariationalSolver(presproblem, 
                                              solver_parameters=
                                              {'ksp_type': 'cg'})


class LinearEadySlice(LinearSlice):

    def __init__(self, setup):

        LinearSlice.__init__(self, setup)

        self.diagnostics_mod = "eady_diagnostics"

        p = self.parameters
        oldu, oldv, oldb, oldp = self.old.vars.split()
        newu, newv, newb, newp = self.new.vars.split()

        # vorticity (eta) space
        V0_elt = TensorProductElement(self.IUh0,self.IUv0)
        self.V0 = FunctionSpace(self.mesh,V0_elt)
        self.old.eta = Function(self.V0)
        self.new.eta = Function(self.V0)

        # background advection
        n = FacetNormal(self.mesh)
        Forcing = Function(oldu.function_space()).project(Expression(('x[1]-H/2','0.'),H=p.H))
        self.uadv = -1./p.f*p.dbdy*Forcing
        self.un = 0.5*(self.uadv[0]*n[0] + abs(self.uadv[0]*n[0]))

        # advection options
        self.field_advection = {
            "uw" : EadyUAdvection(p, oldu, newu, self.old, self.new, self.uinit, self.uadv, self.un,
                                 self.no_normal_flow_bc_ids),
            "v" : EadyVAdvection(p, oldv, newv, self.old, self.new, self.uinit, self.uadv, self.un),
            "b" : EadyBAdvection(p, oldb, newb, self.old, self.new, self.uadv, self.un),
            "p" : EadyPAdvection(p, oldp, newp, self.old, self.new, self.uadv, self.un),
            }

        self.ures = self.field_advection["uw"].residual
        self.vres = self.field_advection["v"].residual
        self.bres = self.field_advection["b"].residual
        self.pres = self.field_advection["p"].residual

    def initialise(self):
        p = self.parameters
        oldu, oldv, oldb, oldp = self.old.vars.split()

        # balanced initialization
        if p.initial_conditions["p"] is "hydrostatic":
            self.hydrostatic_initial_pressure()
            self.pressure_boundary_correction()

        if p.initial_conditions["v"] is "geostrophic":
            self.geostrophic_initial_velocity()
            solve_balanced_velocity(self.parameters,self.old)

        # setup variables and solvers for diagnostics
        if "geostrophic_imbalance" in p.diagnostics.keys():
            self.field_dict["geo"] = Function(oldu.function_space())
            self.diagnostic_fields_solver_dict["geo"] = setup_geostrophic_imbalance_solver(self.parameters,self.old,self.field_dict["geo"])
            p.diagnostics["geostrophic_imbalance"] = [
                "geostrophic_imbalance", 
                {"p": p, "geo": self.field_dict["geo"]}]

