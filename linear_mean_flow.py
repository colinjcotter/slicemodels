from slicemodels import LinearSlice
from firedrake import *


class MeanFlowLinearSlice(LinearSlice):

    def __init__(self,L,H,nlayers,ncolumns,pvertical,phorizontal,
                 dt,nits,alpha,Nsq,csq,f,
                 u0_exp,v0_exp,p0_exp,b0_exp,U):
        self.U = U
        LinearSlice.__init__(self,L,H,nlayers,ncolumns,pvertical,phorizontal,
                 dt,nits,alpha,Nsq,csq,f,
                 u0_exp,v0_exp,p0_exp,b0_exp)
        # Vorticity space
        V0 = self.V0
        self.oldz = Function(V0)
        self.newz = Function(V0)
        # Mesh-related functions
        n = FacetNormal(self.mesh)
        # ( dot(v, n) + |dot(v, n)| )/2.0
        self.un = 0.5*(U*n[0] + abs(U*n[0]))
        self.vfile = File('vort.pvd')

    def SetupVorticitySolver(self):
        # get parameters
        V0 = self.V0
        oldu, oldv = self.oldvel.split()
        # trial and test functions
        z = TrialFunction(V0)
        sigma = TestFunction(V0)
        az = sigma*z*dx
        Lz = -dot(curl(sigma),oldu)*dx
        bc1 = [DirichletBC(V0, Expression("0."), x)
               for x in ["top", "bottom"]]
        zetaproblem = LinearVariationalProblem(az, Lz, self.oldz, bcs=bc1)
        self.zetasolver = LinearVariationalSolver(zetaproblem, 
                                             parameters={'ksp_type': 'cg'})

    def SetupVorticityAdvection(self):
        dt = self.dt
        alpha = self.alpha
        U = self.U
        oldb = self.oldb
        newb = self.newb
        bbar = (1-alpha)*oldb + alpha*newb
        oldz = self.oldz
        # get space
        V0 = self.V0
        # trial and test functions
        z = TrialFunction(V0)
        sigma = TestFunction(V0)
        zbar = alpha*z + (1-alpha)*oldz
        Ezadv = (sigma*(z-oldz) - dt*sigma.dx(0)*(U*zbar + bbar))*dx
        azadv = lhs(Ezadv)
        Lzadv = rhs(Ezadv)
        bc1 = [DirichletBC(V0, Expression("0."), x)
               for x in ["top", "bottom"]]
        zadvproblem = LinearVariationalProblem(azadv, Lzadv, self.newz, bcs=bc1)
        self.zadvsolver = LinearVariationalSolver(zadvproblem, 
                                             parameters={'ksp_type': 'cg'})

    def SetupUResidual(self):
        # get parameters
        dt = self.dt
        alpha = self.alpha
        U = self.U
        # get functions
        newu, newv = self.newvel.split()
        oldu, oldv = self.oldvel.split()
        newp = self.newp
        oldp = self.oldp
        newb = self.newb
        oldb = self.oldb
        newz = self.newz
        oldz = self.oldz
        # get space
        V1 = self.V1
        # set up ubar etc
        ubar = (1-alpha)*oldu + alpha*newu
        pbar = (1-alpha)*oldp + alpha*newp
        bbar = (1-alpha)*oldb + alpha*newb
        zbar = (1-alpha)*oldz + alpha*newz
        # trial and test functions
        du = TrialFunction(V1)
        w = TestFunction(V1)
        self.SetupVorticitySolver()
        self.SetupVorticityAdvection()
        Qperp = -newz*U
        # set velocity residual
        self.ures = Function(V1)
        self.umass = inner(w,du)*dx
        self.Lures = (
            inner(w,newu-oldu) + dt*w[1]*Qperp
            - dt*div(w)*(pbar + U*ubar[0])
            - dt*w[1]*bbar
            )*dx
        bc1 = [DirichletBC(V1, Expression(("0." , "0.")), x)
               for x in ["top", "bottom"]]
        uresproblem = LinearVariationalProblem(self.umass, self.Lures, self.ures,bcs=bc1)
        self.uressolver = LinearVariationalSolver(uresproblem, 
                                             parameters={'ksp_type': 'cg'})


    def SetupPResidual(self):
        # get parameters
        dt = self.dt
        alpha = self.alpha
        csq = self.csq
        U = self.U
        # get functions
        newu, newv = self.newvel.split()
        oldu, oldv = self.oldvel.split()
        newp = self.newp
        oldp = self.oldp
        un = self.un
        # get space
        V2 = self.V2
        # set up ubar etc
        ubar = (1-alpha)*oldu + alpha*newu
        pbar = (1-alpha)*oldp + alpha*newp
        # trial and test functions
        dp = TrialFunction(V2)
        phi = TestFunction(V2)

        self.pmass = phi*dp*dx
        self.Lpres = (phi*(newp-oldp + dt*csq*div(ubar)) - dt*U*(phi.dx(0)*pbar))*dx + dt*dot(jump(phi),(un('+')*pbar('+') - un('-')*pbar('-')))*dS_v
        self.pres = Function(V2)

        presproblem = LinearVariationalProblem(self.pmass, self.Lpres, 
                                               self.pres)
        self.pressolver = LinearVariationalSolver(presproblem, 
                                             parameters={'ksp_type': 'cg'})

    def SetupBResidual(self):
        # get parameters
        dt = self.dt
        alpha = self.alpha
        Nsq = self.Nsq
        U = self.U
        # get functions
        newu, newv = self.newvel.split()
        oldu, oldv = self.oldvel.split()
        newb = self.newb
        oldb = self.oldb
        un = self.un
        # get space
        Vt = self.Vt
        # set up ubar etc
        ubar = (1-alpha)*oldu + alpha*newu
        bbar = (1-alpha)*oldb + alpha*newb
        # trial and test functions
        db = TrialFunction(Vt)
        gamma = TestFunction(Vt)

        self.bmass = gamma*db*dx
        self.Lbres = (gamma*(newb-oldb + dt*Nsq*ubar[1]) - dt*U*gamma.dx(0)*bbar)*dx + dt*dot(jump(gamma),(un('+')*bbar('+') - un('-')*bbar('-')))*dS_v
            
        self.bres = Function(Vt)

        bresproblem = LinearVariationalProblem(self.bmass, self.Lbres, 
                                               self.bres)
        self.bressolver = LinearVariationalSolver(bresproblem, 
                                             parameters={'ksp_type': 'cg'})


    def GetUResidual(self):
        self.zetasolver.solve()
        self.zadvsolver.solve()
        self.uressolver.solve()

