from nonlinear_slice import BAdvection, VAdvection
from utility_functions import *
from firedrake import *
import sys
import numpy as np

class EadyBAdvection(BAdvection):

    def setup_solver_implicit(self):
        print "in setup_solver_implicit in EadyBAdvection"
        dt = self.p.dt
        oldu, oldv, oldb, oldp = self.old.vars.split()
        newu, newv, newb, newp = self.new.vars.split()
        ubar = make_fbar(self.p.alpha, oldu, newu)
        vbar = make_fbar(self.p.alpha, oldv, newv)
        n = self.n
        un = 0.5*(dot(ubar,n) + abs(dot(ubar,n)))
        bbar = (1-self.p.alpha)*self.oldf + self.p.alpha*self.trial
        gammaSU = make_vertical_supg_test_function(
            self.test, ubar, dt, self.advection_options["supg"])
        beqn = (
            (gammaSU*(self.trial - self.oldf + dt*self.p.Nsq*ubar[1] + dt*self.p.dbdy*vbar) 
             + dt*gammaSU*dot(ubar,grad(bbar)))*dx 
            - dt*(gammaSU('+')*dot(ubar('+'),n('+'))*bbar('+')
                  + gammaSU('-')*dot(ubar('-'),n('-'))*bbar('-'))*dS_v 
            + dt*dot(jump(gammaSU),(un('+')*bbar('+') 
                                    - un('-')*bbar('-')))*dS_v
            )
        blhs = lhs(beqn)
        brhs = rhs(beqn)

        bstarproblem = LinearVariationalProblem(blhs, brhs, self.fstar)
        self.fstarsolver = (
            LinearVariationalSolver(bstarproblem, 
                                    solver_parameters={'ksp_type':'gmres',
                                                       'pc_type':'bjacobi',
                                                       'sub_pc_type':'ilu'}))
        self.setup_diffusion()

    def setup_solver_ssprk(self):
        print "in setup_solver_ssprk in EadyBAdvection"
        dt = self.p.dt
        n = self.n
        oldu, oldv, oldb, oldp = self.old.vars.split()
        newu, newv, newb, newp = self.new.vars.split()
        ubar = make_fbar(self.p.alpha, oldu, newu)
        vbar = make_fbar(self.p.alpha, oldv, newv)
        un = 0.5*(dot(ubar,n) + abs(dot(ubar,n)))
        b = self.trial
        gammaSU = make_vertical_supg_test_function(
            self.test, ubar, dt, self.advection_options["supg"])
        blhs = gammaSU*b*dx
        brhs = (
            (gammaSU*(b - dt*self.p.Nsq*ubar[1] - dt*self.p.dbdy*vbar) 
             - dt*gammaSU*dot(ubar,grad(b)))*dx 
            + dt*(gammaSU('+')*dot(ubar('+'),n('+'))*b('+')
                  + gammaSU('-')*dot(ubar('-'),n('-'))*b('-'))*dS_v 
            - dt*dot(jump(gammaSU),(un('+')*b('+') 
                                    - un('-')*b('-')))*dS_v
            )

        bstarproblem = LinearVariationalProblem(blhs,action(brhs,self.f1),
                                                self.fstar)
        self.fstarsolver = LinearVariationalSolver(bstarproblem, 
                                                   solver_parameters
                                                   ={'ksp_type': 'gmres'})
        self.setup_diffusion()


class EadyVAdvection(VAdvection):

    def __init__(self, p, oldf, newf, old, new, advection_options, uinit,
                 oldomega, newomega, no_normal_flow_bc_ids):

        VAdvection.__init__(self, p, oldf, newf, old, new, advection_options, uinit,
                            oldomega, newomega, no_normal_flow_bc_ids)

    def setup_solver_implicit(self):
        print "in setup_solver_implicit in EadyVAdvection"
        dt = self.p.dt
        oldu, oldv, oldb, oldp = self.old.vars.split()
        newu, newv, newb, newp = self.new.vars.split()
        ubar = make_fbar(self.p.alpha, oldu, newu)
        un = 0.5*(dot(ubar,self.n) + abs(dot(ubar,self.n)))
        # Expression for Forcing
        V2 = oldp.function_space()
        Forcing = Function(V2).interpolate(Expression(("x[1]-H/2"),H=self.p.H))
        wv = self.test
        v = self.trial
        vbar = (1-self.p.alpha)*self.oldf + self.p.alpha*v

        veqn = (
            (wv*(v - self.oldf + dt*self.p.f*ubar[0] + dt*self.p.dbdy*Forcing) 
             - dt*div(wv*ubar)*vbar)*dx 
            + dt*dot(jump(wv),(un('+')*vbar('+') - un('-')*vbar('-')))*dS_v 
            + dt*dot(jump(wv),(un('+')*vbar('+') - un('-')*vbar('-')))*dS_h
            )
        vlhs = lhs(veqn)
        vrhs = rhs(veqn)
        vstarproblem = LinearVariationalProblem(vlhs, vrhs, self.fstar)
        self.fstarsolver = LinearVariationalSolver(vstarproblem, 
                                                   solver_parameters=
                                                   {'ksp_type': 'cg'})

    def setup_solver_ssprk(self):
        print "in setup_solver_ssprk in EadyVAdvection"
        dt = self.p.dt
        oldu, oldv, oldb, oldp = self.old.vars.split()
        newu, newv, newb, newp = self.new.vars.split()
        ubar = make_fbar(self.p.alpha, oldu, newu)
        un = 0.5*(dot(ubar,self.n) + abs(dot(ubar,self.n)))
        # Expression for Forcing
        V2 = oldp.function_space()
        Forcing = Function(V2).interpolate(Expression(("x[1]-H/2"),H=self.p.H))
        wv = self.test
        v = self.trial

        vlhs = wv*v*dx
        vrhs = (
            (wv*(v - dt*self.p.f*ubar[0] - dt*self.p.dbdy*Forcing) 
             + dt*div(wv*ubar)*v)*dx 
            - dt*dot(jump(wv),(un('+')*v('+') - un('-')*v('-')))*dS_v 
            - dt*dot(jump(wv),(un('+')*v('+') - un('-')*v('-')))*dS_h
            )
        vstarproblem = LinearVariationalProblem(vlhs, 
                                                action(vrhs,self.f1), 
                                                self.fstar)
        self.fstarsolver = LinearVariationalSolver(vstarproblem, 
                                                   solver_parameters=
                                                   {'ksp_type': 'cg'})
